<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Program extends Model
{
    use SoftDeletes;
    protected $table = 'program';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'program_name', 'category_id', 'description', 'classification_id', 'level', 'attachement', 'status','duration'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    protected $dates = ['deleted_at'];

    public function classification(){
        return $this->belongsTo('App\Classification','classification_id','id');
    }

    public function category(){
        return $this->belongsTo('App\Category','category_id','id');
    }
}
