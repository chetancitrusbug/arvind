<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeProgram extends Model
{
    use SoftDeletes;
    protected $table = 'employee_program';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'program_id', 'employee_id',  'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    protected $dates = ['deleted_at'];

    public function program(){
        return $this->belongsTo('App\Program','id','program_id');
    } 

    public function employee(){
        return $this->belongsTo('App\Employee','id', 'employee_id');
    } 

}
