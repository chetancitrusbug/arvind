<?php

namespace App\Http\Middleware;
use App\Notification;
use Closure;
use Session;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      
        if ($request->user()->hasRole('SU') || $request->user()->hasRole('manager') || $request->user()->hasRole('employee') ) {
            $totalNotification = Notification::select('notification.*','notification.id as notification_id','batch.*')->join('batch','notification.batch_id','batch.id')->where('notification.read_status', 0)->get();
            //dd($totalNotification);
            view()->composer('*', function ($view) use ($totalNotification) {
                $view->with(compact('totalNotification'));
            });
            return $next($request);

        }
        else if ($request->user()->hasRole('batch')) {
            return redirect()->to('/batchuser/batch');
        }

        
        return redirect()->to('/');

    }
}
