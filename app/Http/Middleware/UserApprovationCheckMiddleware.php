<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class UserApprovationCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::user()->active == 0) {

            flash('Account not activated. Please check your email.', 'error');

            return redirect()->back();
        }


        if (Auth::user()->status == 'unapproved') {

            flash('Account not approved by admin.', 'error');

            return redirect()->back();
        }


        return $next($request);
    }
}
