<?php

namespace App\Http\Controllers\Batch;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Batch;
use App\BatchNominee;
use App\FeedbackQuestion;
use App\FeedbackCategory;
use App\FeedbackBatch;
use App\Employee;
use Session;
use Auth;
Use Crypt;

class FeedbackController extends Controller
{
    public function feedbackForm(Request $request)
    {
        $feedbackQuestionCount = FeedbackBatch::where('employee_id',$request->employee_id)->where('batch_id',$request->batch_id)->get();
        if(count($feedbackQuestionCount) > 0)
        {
            return view('admin.batch.successorfail',['msg' => 'You have already applied!' ]);
        }
        $data = $request->all();
        $feedbackCategory = FeedbackCategory::where('status',1)->get();
        $feedbackQuestion = FeedbackQuestion::with('feedbackCategory')->where('status',1)->get();
        
        return view('batch.batch.feedbackform',compact('data','feedbackQuestion','feedbackCategory'));
    }
    public function feedbackStore(Request $request)
    {
        $reqData = $request->all();
        //dd($reqData);
        $feedbackQuestionCount = FeedbackBatch::where('employee_id',$reqData['employee_id'])->where('batch_id',$reqData['batch_id'])->get();
        if(count($feedbackQuestionCount) > 0)
        {
            return view('admin.batch.successorfail',['msg' => 'You have already applied!' ]);
        }
        $feedbackQuestion = FeedbackQuestion::with('feedbackCategory')->where('status',1)->get();
        $data = array();
        foreach ($feedbackQuestion as $key => $value) {
            $data[$key]['feedback_question_id'] = $value->id;
            $data[$key]['feedback_answer'] = isset($reqData['question'.$value->id]) ? $reqData['question'.$value->id] : '';
            $data[$key]['batch_id'] = isset($reqData['batch_id']) ? $reqData['batch_id'] : '';
            $data[$key]['employee_id'] = isset($reqData['employee_id']) ? $reqData['employee_id'] : '';
        }
        
        $feedbackBatch = FeedbackBatch::insert($data);

        $employee = Employee::where('id', $reqData['employee_id'])->first();
        $batch = Batch::with('program')->where('id', $reqData['batch_id'])->first();
        if(isset($batch->program->attachement) && $batch->program->attachement != null)
        $pathToFile = url($batch->program->attachement);
        else
        $pathToFile = '';
        \Mail::send('email.BatchCompleted', ['reqData' =>$reqData,'userName'=>$employee->emp_name,'pathToFile'=>$pathToFile], function ($message) use ($employee) {
            $message
                ->to($employee->email)
                ->subject('Batch Completed');                   
            });
        return view('admin.batch.successorfail',['msg' => 'Thank you for response!' ]);
    }
    public function viewFeedback(Request $request,$id)
    {
        try {
            $id = Crypt::decrypt($id);
            $batch = Batch::with('program','vendor','trainer')->where('id',$id)->first();
            if($batch == null)
            {
                return view('admin.batch.successorfail',['msg' => ' Batch is not exist!' ]);
            }
            $batch->programName = ((isset($batch->program->program_name) ? $batch->program->program_name : ''));
            $batch->vendorName = ((isset($batch->vendor->vendor_name) ? $batch->vendor->vendor_name : ''));
            $batch->trainnerName = ((isset($batch->trainer->trainer_name) ? $batch->trainer->trainer_name : ''));
            return view('batch.batch.feedback',compact('batch'));
        }
        catch (\Exception $e) {
            //$e->getMessage()
            return view('admin.batch.successorfail',['msg' => 'Batch is not exist!' ]);
        }
    }
    public function getEmployee(Request $request)
    {
        $employee = Employee::with('department')->where('emp_code', $request->emp_code)->first();
        if($employee == null)
        {
            $message='Employee not exist!';
            return response()->json(['message'=>$message],200);
        }
        $batch_id = $request->batchId;
        $html = view('batch.batch.employeeForm',compact('employee','batch_id'))->render();
        return response()->json(['data' => $html,'message' => 'Success!'],200);
        
    }
    
}
