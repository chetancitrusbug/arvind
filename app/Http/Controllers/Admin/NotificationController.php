<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notification;
use Session;
use Yajra\Datatables\Datatables;

class NotificationController extends Controller
{
    public function index(Request $request)
    {

        return view('admin.notification.index');
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $notification = Notification::select('notification.*','batch.*')->join('batch','notification.batch_id','batch.id')->where('notification.read_status', 0);

        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(batch.id LIKE  '%$value%' OR batch.date LIKE  '%$value%' )";
                $notification = $notification->whereRaw($where_filter);
            }
        }
        $notification = $notification->orderby('notification.id','desc')->get();

        return Datatables::of($notification)
            ->make(true);
        exit;
    }

    public function readStatus( Request $request)
    {
        $notification = Notification::where('id', $request->notification_id)->first();
        if($notification == NULL) {
            $message='Notification is not exist!';
            return response()->json(['message'=>$message],200);
        }
        $notification->update(['read_status' => $request->action ]);

        $message='Mark as Read!';
        
        return response()->json(['message'=>$message],200);

    }

}
