<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vendor;
use App\Trainer;
use Session;
use Yajra\Datatables\Datatables;

class VendorController extends Controller
{
    public function index(Request $request)
    {

        return view('admin.vendor.index');
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(vendor.vendor_code LIKE  '%$value%' OR vendor.vendor_name LIKE  '%$value%' OR vendor.email LIKE  '%$value%'  )";
                $vendor = Vendor::whereRaw($where_filter)->orderby('id','desc')->get();
            }
            else{
                $vendor = Vendor::orderby('id','desc')->get();
            }
        }
        else{
            $vendor = Vendor::orderby('id','desc')->get();
        }

        return Datatables::of($vendor)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {       
        return view('admin.vendor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'vendor_name' => 'required',
            'mobile' => 'required|digits_between:10,12',
            'email' => ['required', \Illuminate\Validation\Rule::unique('vendor', 'email')->whereNull('deleted_at')],
            'vendor_code' => ['required', \Illuminate\Validation\Rule::unique('vendor', 'vendor_code')->whereNull('deleted_at')],
            'address1' => 'required',
            'address2' => 'required',
            'status' => 'required',
        ]);
       
        $data = array(
            'vendor_name' => $request->input('vendor_name'),
            'mobile' => $request->input('mobile'),
            'address1' => $request->input('address1'),
            'address2' => $request->input('address2'),
            'email' => $request->input('email'),
            'vendor_code' => $request->input('vendor_code'),
            'status' => $request->input('status'));

        $vendor = Vendor::where('email', $request->input('email'))->withTrashed()->first();
        if($vendor == null)
        {
            $vendor = Vendor::create($data);
        }
        else
        {   
            $vendor->restore();
            $vendor->update($data);            
        }
        
        //Welcome Mail Send
        \Mail::send('email.welcome', ['name' => $vendor->vendor_name], function ($message) use ($vendor) {
            $message
            ->to($vendor['email'])
            ->subject('Welcome to Arvind University');                           
        });
        Session::flash('flash_message', 'Vendor added!');
        return redirect('admin/vendor');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $vendor = Vendor::where('id', $id)->first();
        
        if ($vendor) {
            return view('admin.vendor.edit', compact('vendor'));
        } else {
            Session::flash('flash_message', 'vendor is not exist!');
            return redirect('admin/vendor');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'vendor_name' => 'required',
            'mobile' => 'required|digits_between:10,12',
            'address1' => 'required',
            'address2' => 'required',
            'status' => 'required',
        ]);
       
        $requestData = array(
            'vendor_name' => $request->input('vendor_name'),
            'mobile' => $request->input('mobile'),
            'address1' => $request->input('address1'),
            'address2' => $request->input('address2'),
            'status' => $request->input('status'));
        
        $vendor = Vendor::where('id', $id);
        $vendor->update($requestData);
        Session::flash('flash_message', 'Vendor Updated Successfully!');
        return redirect('admin/vendor');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    
    public function destroy(Request $request, $id)
    {
        $vendor = Vendor::where('id', $id);
        $vendor->delete();
        $message='vendor Deleted';
        return response()->json(['message'=>$message],200);

    }

   
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {
        $vendor = Vendor::where('id', $id)->first();
        if($vendor == NULL) {
            Session::flash('flash_message', 'Vendor is not exist!');
            return redirect('admin/vendor');
        }
        //change client status
        $status = $request->get('status');
        if($status != ''){
            if($status == '0' ){
                $vendor->status= '0';
                $vendor->update();            
                Session::flash('flash_message', 'Vendor Status change to inactive');
            }else{
                $vendor->status= '1';
                $vendor->update();               
                Session::flash('flash_message', 'Vendor Status change to active');
            }

        }
        return redirect('admin/vendor');
    }

    public function listTrainer(Request $request,$id)
    {

        return view('admin.vendor.listTrainers',compact('id'));
    }
    
    public function listTrainerDatatable(request $request,$id)
    {
        $trainer = Trainer::where('trainers.vendor_id',$id);

        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(trainers.trainer_code LIKE  '%$value%' OR trainers.trainer_name LIKE  '%$value%' OR trainers.email LIKE  '%$value%' )";
                $trainer = $trainer->whereRaw($where_filter);
            }
        }
        
        $trainer = $trainer->orderby('trainers.id','desc')->get();

        return Datatables::of($trainer)
            ->make(true);
        exit;
    }
}
