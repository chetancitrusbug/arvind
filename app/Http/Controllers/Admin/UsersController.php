<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\Location;
use Illuminate\Http\Request;
use Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return redirect('admin/employee');
        //return view('admin.users.index');
    }

    public function datatable(Request $request)
    {   
        
        $users = User::all();
       
        return datatables()->of($users)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
            'phone' => 'required|digits_between:10,12',
            'status' => 'required'
        ]);

        $data = $request->except('password');
        $data['password'] = bcrypt($request->password);
        $user = User::create($data);

        $user->assignRole('SU');

        Session::flash('flash_success', 'User added!');

        return redirect('admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        $user = User::where('id',$id)->first();
        if ($user) {
            return view('admin.users.show', compact('user'));
        } else {
            Session::flash('flash_message', 'User is not exist!');
            return redirect('admin/users');
        }        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
		$user = User::where('id',$id)->first();
        if ($user) {
            return view('admin.users.edit', compact('user'));
        } else {
            Session::flash('flash_warning', 'User is not exist!');
            return redirect('admin/users');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required|digits_between:10,12',
            'status' => 'required'
        ]);

        $requestData = $request->all();              
        $user = User::where('id',$id)->first();
        $user->update($requestData);
        // $user->roles()->detach();

        // $user->assignRole('SU');
        Session::flash('flash_success', 'User updated!');

        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->roles()->sync([]);

        $user->delete();

        $message = "User Deleted !!";

        return response()->json(['message' => $message],200);
    }
}
