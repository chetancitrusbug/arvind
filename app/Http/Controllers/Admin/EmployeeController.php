<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Brand;
use App\Location;
use App\Department;
use App\Designation;
use App\Division;
use App\Employee;
use App\Region;
use App\Program;
use App\EmployeeProgram;
use App\BatchNominee;
use App\Category;
use App\Classification;
Use App\User;
use Session;
use Auth;
use Yajra\Datatables\Datatables;


class EmployeeController extends Controller
{
    public function index(Request $request)
    {   

        return view('admin.employee.index');
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {   
        $employee = [];
        if(Auth::user()->hasRole('manager') && Auth::user()->employee ){
            $employee = Employee::where('reporting_manager', Auth::user()->employee->emp_code)->with('emp_programs')->select('employee.*','department.department_name')->join('department','employee.department_id','department.id');
        }else{
            $employee = Employee::select('employee.*','department.department_name')->join('department','employee.department_id','department.id');
        }
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(employee.emp_code LIKE  '%$value%' OR employee.emp_name LIKE  '%$value%' OR employee.email LIKE  '%$value%' OR department.department_name  LIKE  '%$value%')";
                $employee = $employee->whereRaw($where_filter);
            }
        }
        $employee = $employee->orderby('employee.id','desc')->get();

        return Datatables::of($employee)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {

        $brand = Brand::where('status','1')->pluck('brand_name','id')->prepend('Select Brand',''); 
        $location = Location::where('status','1')->pluck('location_name','id')->prepend('Select Location','');
        $region = Region::where('status','1')->pluck('region_name','id')->prepend('Select Region','');
        $division = Division::where('status','1')->pluck('division_name','id')->prepend('Select Division','');
        $designation = Designation::where('status','1')->pluck('designation_name','id')->prepend('Select Designation','');
        $department = Department::where('status','1')->pluck('department_name','id')->prepend('Select Department','');
        $program = Program::where('status','1')->pluck('program_name','id');
        
        return view('admin.employee.create',compact('brand','location','region','division','designation','department','program'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'emp_name' => 'required',
            'mobile_no' => 'required|digits_between:10,12',
            'reporting_manager' => 'required',
            'email' => //'required|unique:employee',
            ['required', \Illuminate\Validation\Rule::unique('employee', 'email')->whereNull('deleted_at')],
            'emp_code' => ['required', \Illuminate\Validation\Rule::unique('employee', 'emp_code')->whereNull('deleted_at')],
            'level' => 'required|in:1,2,3',
            'region_id' => 'required',
            'department_id' => 'required',
            'designation_id' => 'required',
            'brand_id' => 'required',
            'division_id' => 'required',
            'location_id' => 'required',
            'program' => 'required',
            'status' => 'required',
        ]);
       
        $employee = Employee::where('email', $request->input('email'))->withTrashed()->first();
        
        $data = array(
            'emp_name' => $request->input('emp_name'),
            'mobile_no' => $request->input('mobile_no'),
            'reporting_manager' => $request->input('reporting_manager'),
            'email' => $request->input('email'),
            'emp_code' => $request->input('emp_code'),
            'level' => $request->input('level'),
            'region_id' => $request->input('region_id'),
            'department_id' => $request->input('department_id'),
            'designation_id' => $request->input('designation_id'),
            'brand_id' => $request->input('brand_id'),
            'division_id' => $request->input('division_id'),
            'location_id' => $request->input('location_id'),
            'status' => $request->input('status')
        );
        if($employee == null)
        {
            $employee = Employee::create($data);
        }
        else
        {   
            $employee->restore();
            $employee->update($data);
            EmployeeProgram::where('employee_id',$employee->id)->where('status','1')->delete();
        }
        if($employee != null)
        {
            foreach ($request->program as $key => $value) {
                $employeeData['employee_id'] = $employee->id;
                $employeeData['program_id'] = $value;
                EmployeeProgram::create($employeeData);
            }
        }
        //Welcome Mail Send
        \Mail::send('email.welcome', ['name' => $employee->emp_name], function ($message) use ($employee) {
            $message
            ->to($employee['email'])
            ->subject('Welcome to Arvind University');                           
        });
        Session::flash('flash_message', 'Employee added!');
        return redirect('admin/employee');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $employee = Employee::where('id', $id)->first();
        $brand = Brand::where('status','1')->pluck('brand_name','id')->prepend('Select Brand',''); 
        $location = Location::where('status','1')->pluck('location_name','id')->prepend('Select Location','');
        $region = Region::where('status','1')->pluck('region_name','id')->prepend('Select Region','');
        $division = Division::where('status','1')->pluck('division_name','id')->prepend('Select Division','');
        $designation = Designation::where('status','1')->pluck('designation_name','id')->prepend('Select Designation','');
        $department = Department::where('status','1')->pluck('department_name','id')->prepend('Select Department','');
        $program = Program::where('status','1')->pluck('program_name','id');
        $employee->program = EmployeeProgram::where('employee_id',$id)->where('status','1')->pluck('program_id');
       
        if ($employee) {
            return view('admin.employee.edit', compact('employee','brand','location','region','division','designation','department','program'));
        } else {
            Session::flash('flash_message', 'Employee is not exist!');
            return redirect('admin/employee');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'emp_name' => 'required',
            'mobile_no' => 'required|digits_between:10,12',
            'reporting_manager' => 'required',
            'level' => 'required|in:1,2,3',
            'region_id' => 'required',
            'department_id' => 'required',
            'designation_id' => 'required',
            'brand_id' => 'required',
            'division_id' => 'required',
            'location_id' => 'required',
            'program' => 'required',
            'status' => 'required',
        ]);
       
        $requestData = array(
            'emp_name' => $request->input('emp_name'),
            'mobile_no' => $request->input('mobile_no'),
            'reporting_manager' => $request->input('reporting_manager'),
            'level' => $request->input('level'),
            'region_id' => $request->input('region_id'),
            'department_id' => $request->input('department_id'),
            'designation_id' => $request->input('designation_id'),
            'brand_id' => $request->input('brand_id'),
            'division_id' => $request->input('division_id'),
            'location_id' => $request->input('location_id'),
            'status' => $request->input('status'));
        
        $employee = Employee::where('id', $id);
        $employee->update($requestData);

        EmployeeProgram::where('employee_id',$id)->where('status','1')->delete();
        foreach ($request->program as $key => $value) {
            $employeeData['employee_id'] = $id;
            $employeeData['program_id'] = $value;
            EmployeeProgram::create($employeeData);
        }

        Session::flash('flash_message', 'Employee Updated Successfully!');
        return redirect('admin/employee');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    
    public function destroy(Request $request, $id)
    {
        $employee = Employee::where('id', $id);
        $employee->delete();
        $message='Employee Deleted';
        return response()->json(['message'=>$message],200);

    }

   
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {
        $employee = Employee::where('id', $id)->first();
        if($employee == NULL) {
            Session::flash('flash_message', 'Employee is not exist!');
            return redirect('admin/employee');
        }
        //change client status
        $status = $request->get('status');
        if($status != ''){
            if($status == 0 ){
                $employee->status= 0;
                $employee->update();            
                Session::flash('flash_message', 'Employee Status change to Inactive');
            }else{
                $employee->status= 1;
                $employee->update();               
                Session::flash('flash_message', 'Employee Status change to Active');
            }
         
        }
        return redirect('admin/employee');
    }

    
    public function getLocation(Request $request,$id)
    {
        $location = Location::where('region_id', $id)->where('status','1')->pluck('location_name','id'); 
        $location = json_encode($location);
        return $location;
    }

    public function listProgram(Request $request,$id)
    {
        return view('admin.employee.listProgram',compact('id'));
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function listProgramDatatable(request $request,$id)
    {

        $program =  EmployeeProgram::select('program.*','classification.classification_name','category.cat_name','employee_program.employee_id','employee_program.program_id')
        ->join('program','employee_program.program_id','program.id')
        ->join('classification','program.classification_id','classification.id')
        ->join('category','program.category_id','category.id')
        ->where('employee_program.employee_id',$id);

        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(program.program_name LIKE  '%$value%' OR category.cat_name LIKE  '%$value%' OR classification.classification_name LIKE  '%$value%')";
                $program = $program->whereRaw($where_filter);
            }
        }
        

        $program = $program->orderby('employee_program.id','desc')->distinct()->get();
        return Datatables::of($program)
            ->make(true);
        exit;
    }

    public function listBatch(Request $request,$id)
    {
        return view('admin.employee.listBatch',compact('id'));
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function listBatchDatatable(request $request,$id)
    {

        $batch =  BatchNominee::select('batch.*','trainers.trainer_name','vendor.vendor_name','program.program_name','batch_nominee.batch_id','batch_nominee.employee_id')
        ->join('batch','batch_nominee.batch_id','batch.id')
        ->join('trainers', 'batch.trainer_id', '=', 'trainers.id')
        ->join('vendor', 'batch.vendor_id', '=', 'vendor.id')
        ->join('program', 'batch.program_id', '=', 'program.id')
        ->where('batch_nominee.employee_id',$id);

        
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(batch.id LIKE  '%$value%' OR batch.date LIKE  '%$value%' OR vendor.vendor_name LIKE  '%$value%' OR trainers.trainer_name LIKE  '%$value%' OR program.program_name LIKE  '%$value%'  )";
                $batch = $batch->whereRaw($where_filter);
            }
        }
        $batch = $batch->orderby('batch.id','desc')->distinct()->get();

        return Datatables::of($batch)
            ->make(true);
        exit;
    }

    public function employeePrograms(request $request, $id){

        if(Auth::user()->hasRole('manager')){
            
            $employee = Employee::with('programs')->find($id);
            $selected_programs = [];
            $selected_programs = $employee->programs->pluck('id')->toArray();
            $category = Category::with(['program'=>function($program)use($selected_programs){
                    $program->whereIn('id', $selected_programs)->get();
            }])->get();

            $select_status = EmployeeProgram::whereIn('program_id',$selected_programs)->where('employee_id',$employee->id)->groupBy('parent_id')->pluck('status')->first(); 

            if($request->has('status') &&  $request->get('status') != ""){
                $status = $request->get('status') ;
                if($status == "approve"){

                    EmployeeProgram::whereIn('program_id',$selected_programs)->where('employee_id',$employee->id)->where('status', 1)->update(['status' => 3]);

                    Session::flash('flash_message', 'Programs request of '.$employee->emp_name.' has been Approved!');
                }
                if($status == "denied"){

                    EmployeeProgram::whereIn('program_id',$selected_programs)->where('employee_id',$employee->id)->where('status', 1)->update(['status' => 2]);

                    Session::flash('flash_message', 'Programsn request of '.$employee->emp_name.' has been Denied!');
                }
                return redirect()->back();
            }
            return view('admin.employee.program', compact('employee','category','select_status'));
        }
        return redirect()->back();

    }

    public function uploadeExcel()
    {
        return view('admin.employee.uploadeexcel');
    }

    public function upload(Request $request)
    {   

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 10000000);

        $this->validate($request, [
            'uploadExcel' => 'required|mimes:xlsx,xls,xlsm,xlsb'
        ]);
        if ($request->hasFile('uploadExcel')) {
            $path = $request->file('uploadExcel')->getRealPath();
            $data = \Excel::load($path)->get();   // get sheet collections

            if ($data->count()) {

                // inactive all the employees 
                $employees = Employee::get();
                foreach($employees  as $employee_user){
                    $employee_user->user()->update(['status'=> 0 ]);
                    $employee_user->update(['status'=> 0 ]);
                }        
				$reportingManagerCode = [];
                foreach ($data as $key => $valueCheck) {
                    $goOn = true;
                    if (is_object($valueCheck)) {
                        foreach ($valueCheck as $dataValue) {
                            if ($dataValue == '') {
                                $goOn = false;
                            }
                        }
                    } else {
                        $goOn = false;
                    }
                    if ($goOn) {
                        foreach ($valueCheck as $key2 => $value) {
                            $goOnSecond = true;
                            if (is_object($value)) {
                                foreach ($value as $dataValue) {
                                    if ($dataValue == '') {
                                        $goOnSecond = false;
                                    }
                                    if( isset($value['reporting_manager_ecode']) && trim($value['reporting_manager_ecode']) == '' ){
                                        $goOnSecond = true;
                                    }
                                }
                            } else {
                                $goOnSecond = false;
                            }
                            if ($goOnSecond) {
                                $employeeCode = trim($value['employee_code']);
                                $employeeName = trim($value['employee_full_name']);
                                $level = trim($value['level']);
                                $grade = trim($value['grade']);
                                $designation = trim($value['designation']);
                                $department = trim($value['department']);
                                $brand = trim($value['brand']);
                                $division = trim($value['division']);
                                $location = trim($value['city']).' , '.trim($value['state']);
                                $region = trim($value['region']);
                                $reportingManagerEmpCode =  trim($value['reporting_manager_ecode']);
                                $employeeEmailId = trim($value['email_id']);
                                $reportingManagerEmpCode =  trim($value['reporting_manager_ecode']);
								$reportingManagerCode[] = trim($value['reporting_manager_ecode']);
                                // $nominationStatus = trim($value['nomination_status']);
                                // $attendance = trim($value['attendance']);
                                
                                // $programName = trim($value['programname']);
                                // $programCategory = trim($value['program_category']);
                                // $classification = trim($value['classification']);
                               
                                //$reportingManagerEmailId = trim($value['reportingmanageremailid']); 
                                  
                                if($level){
                                    $level = substr($level, -1);
                                    if($level == '1'){
                                        $level = 1;
                                    }
                                    if ($level == '2') {
                                        $level = 2;
                                    }
                                    if ($level == '3') {
                                        $level = 3;
                                    }

                                }
								// Check Classification 
                                // $getClassification = \App\Classification::where('classification_name', $classification)->get();
                                // if (count($getClassification) > 0) {
                                //     $calssificationId = $getClassification[0]->id;
                                // } else {
                                //     $classification = \App\Classification::create(array('classification_name' => $classification, 'status' => 1));
                                //     $calssificationId = $classification->id;
                                // }  
                                
                                // don't make enrty fro program and category on employee excel upload
                                    // // Check Category
                                    // $getCategory = \App\Category::where('cat_name', $programCategory)->get();
                                    // if (count($getCategory) > 0) {
                                    //     $categoryId = $getCategory[0]->id;
                                    // } else {
                                    //     $category = \App\Category::create(array('cat_name' => $programCategory, 'classification_id' => $calssificationId, 'status' => 1));
                                    //     $categoryId = $category->id;
                                    // }
                                    // // Check Programe
                                    // $getPrograme = \App\Program::where('program_name', $programName)->where('category_id', $categoryId)->where('classification_id', $calssificationId)->get();
                                    // if (count($getPrograme) > 0) {
                                    //     $programeId = $getPrograme[0]->id;
                                    // } else {
                                    //     $data = array(
                                    //         'program_name' => $programName,
                                    //         'classification_id' => $calssificationId,
                                    //         'category_id' => $categoryId,
                                    //         'level' => $level,
                                    //         'status' => 1
                                    //     );
                                    //     $programe = \App\Program::create($data);
                                    //     $programeId = $programe->id;
                                    // }

								// Check Division
                                $getDivision = Division::where('division_name', $division)->get();
                                if (count($getDivision) > 0) {
                                    $devisionId = $getDivision[0]->id;
                                } else {
                                    $data = array(
                                        'division_name' => $division,
                                        'status' => 1
                                    );
                                    $division = Division::create($data);
                                    $devisionId = $division->id;
                                }
								// Check Region
                                $getRegion = Region::where('region_name', $region)->get();
                                if (count($getRegion) > 0) {
                                    $regionId = $getRegion[0]->id;
                                } else {
                                    $data = array(
                                        'region_name' => $region,
                                        'status' => 1
                                    );
                                    $region = Region::create($data);
                                    $regionId = $region->id;
                                }
								// Check Location
                                $getLocation = Location::where('location_name', $location)->get();
                                if (count($getLocation) > 0) {
                                    $locationId = $getLocation[0]->id;
                                } else {
                                    $data = array(
                                        'location_name' => $location,
                                        'region_id' => $regionId,
                                        'status' => 1
                                    );
                                    $location = Location::create($data);
                                    $locationId = $location->id;
                                }
								// Check Department
                                $getDepartment = Department::where('department_name', $department)->get();
                                if (count($getDepartment) > 0) {
                                    $departmentId = $getDepartment[0]->id;
                                } else {
                                    $data = array(
                                        'department_name' => $department,
                                        'status' => 1
                                    );
                                    $department = Department::create($data);
                                    $departmentId = $department->id;
                                }
								
								// Check Designation
                                $getDesignation = Designation::where('designation_name', $designation)->get();
                                if (count($getDesignation) > 0) {
                                    $designationId = $getDesignation[0]->id;
                                } else {
                                    $data = array(
                                        'designation_name' => $designation,
                                        'status' => 1
                                    );
                                    $designation = Designation::create($data);
                                    $designationId = $designation->id;
                                }

								// Check Brand
                                $getBrand = Brand::where('brand_name', $brand)->get();
                                if (count($getBrand) > 0) {
                                    $brandId = $getBrand[0]->id;
                                } else {
                                    $data = array(
                                        'brand_name' => $brand,
                                        'status' => 1
                                    );
                                    $brand = Brand::create($data);
                                    $brandId = $brand->id;
                                }

								//Check Employee
                                $getEmployee = Employee::where('emp_code', $employeeCode)->whereOr('email',$employeeEmailId)->first();
                                if ($getEmployee) {
									//dd($getEmployee);
									$getEmployee->user()->update(['status'=> 1 ]);
                                    $getEmployee->update(['status' => 1,'level'=>$level]);
									$employeeId = $getEmployee->id;
                                } else {
                                    if($employeeEmailId){
                                        $data = array(
                                            'emp_code' =>  $employeeCode,
                                            'emp_name' => $employeeName,
                                            'reporting_manager' =>   $reportingManagerEmpCode ,
                                            'email' => $employeeEmailId,
                                            'level' => $level,
                                            'region_id' => $regionId,
                                            'department_id' => $departmentId,
                                            'designation_id' => $designationId,
                                            'brand_id' => $brandId,
                                            'division_id' => $devisionId,
                                            'location_id' => $locationId,
                                            'status' => 1
                                        );
                                        //check for the unique emp code 
                                        $unique_emp = Employee::where('emp_code', $employeeCode)->first();
                                        if($unique_emp){
                                            $data['emp_code'] = str_replace(' ', '', substr(str_shuffle(str_repeat("0123456789" . $employeeName, 5)), 0, 5)) . str_replace(' ', '', substr(str_shuffle(str_repeat("0123456789" . $employeeName, 5)), 0, 5)) ;   // generate a new unique emp code
                                        }     
                                        $emaployee = Employee::create($data);
                                        $employeeId = $emaployee->id;
                                    }
                                    
                                }

                                // make a user  & generate password 
                                $emp =  Employee::find($employeeId);
                                if($emp && ( $emp->user_id == null || $emp->user_id == "" ) ){
                                    $user = User::where('email', $emp->email)->first();
                                    if(!$user){
                                        $user_data['name'] = $emp->emp_name;
                                        $user_data['email'] = $emp->email;
                                        $pwd = $emp->emp_code.'@12345'; 
                                        $user_data['password'] = bcrypt($pwd);
                                        $user_data['status'] = 1;
                                        $user = User::create($user_data);
                                        $emp->user_id = $user->id;
                                        $emp->save();
										$user->assignRole('employee');
                                    }
                                    // // if reportingManagerEmpCode is blank then assign them a role of manager
                                    // if( $reportingManagerEmpCode == "" || !$reportingManagerEmpCode || empty($reportingManagerEmpCode) ){
                                    //     $user->assignRole('manager');
                                    // }else{
                                    //     $user->assignRole('employee');
                                    // }

                                }

                                // no relation for employee and program from employee ecxel upload

                                // $getRelationEmployeeWithProgram = \App\EmployeeProgram::where('program_id', $programeId)->where('employee_id', $employeeId)->get();
                                // if (!(count($getRelationEmployeeWithProgram) > 0)) {
                                //     $data = array(
                                //         'program_id' => $programeId,
                                //         'employee_id' => $employeeId,
                                //         'status' => 1
                                //     );
                                //     $emaployeeProgram = \App\EmployeeProgram::create($data);
                                //     $emaployeeProgramId = $emaployeeProgram->id;
                                // }
                            }
                        }
                    }
                }
				if(!empty($reportingManagerCode)){
					$employee = Employee::whereIn('emp_code',$reportingManagerCode)->select('user_id');
					$role = \DB::table('role_user')->whereIn('user_id',$employee)->update(['role_id'=>4]);
					//$role = Role::where('user_id',$employee)->update(['role_id'=>4]);
				}
					
                Session::flash('flash_message', 'Excel Uploaded Successfully');
                //echo '</pre>';
            } else {
                Session::flash('flash_message', 'Excel can not be Uploaded');
            }
            return redirect('admin/employee');
        }

    }

}
