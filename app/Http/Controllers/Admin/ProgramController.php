<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classification;
use App\Category;
use App\Program;
Use App\User;
use App\EmployeeProgram;
use \App\Division;
use \App\Region;
use \App\Location;
use \App\Department;
use \App\Brand;
use \App\Designation;
use \App\Employee;
use Yajra\Datatables\Datatables;
use Session;
Use Auth;

class ProgramController extends Controller
{
    public function index(Request $request)
    {

        return view('admin.program.index');
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {

        $program = Program::select('program.*', 'classification.classification_name', 'category.cat_name')
            ->join('classification', 'program.classification_id', 'classification.id')
            ->join('category', 'program.category_id', 'category.id');

        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');

            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(program.program_name LIKE  '%$value%' OR category.cat_name LIKE  '%$value%' OR classification.classification_name LIKE  '%$value%')";
                $program = $program->whereRaw($where_filter);
            }
        }


        $program = $program->orderby('program.id', 'desc')->get();

        return Datatables::of($program)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {

        //$category = Category::where('status','1')->pluck('cat_name','id')->prepend('Select Category',''); 
        $classification = Classification::where('status', '1')->pluck('classification_name', 'id')->prepend('Select Classification', '');

        return view('admin.program.create', compact('classification'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'program_name' => 'required',
            'level' => 'required|in:1,2,3',
            'classification_id' => 'required',
            'category_id' => 'required',
            'duration' => 'required',
            'attachement' => 'required|mimes:zip,rar'
        ]);

        $attachement = '';
        if ($request->file('attachement')) {
            $fimage = $request->file('attachement');
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('/uploads/program/'), $filename);
            $attachement = '/uploads/program/' . $filename;
        }

        $data = array(
            'program_name' => $request->input('program_name'),
            'classification_id' => $request->input('classification_id'),
            'category_id' => $request->input('category_id'),
            'duration' => $request->input('duration'),
            'level' => $request->input('level'),
            'attachement' => $attachement
        );

        if ($request->input('description')) {
            $data['description'] = $request->input('description');
        }
        $program = program::create($data);

        Session::flash('flash_message', 'Program added!');
        return redirect('admin/program');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $program = program::where('id', $id)->first();

        $classification = Classification::where('status', '1')->pluck('classification_name', 'id')->prepend('Select Classification', '');


        if ($program) {
            $category = Category::where('classification_id', $program->classification_id)->where('status', '1')->pluck('cat_name', 'id')->prepend('Select Category', '');
            return view('admin.program.edit', compact('program', 'category', 'classification'));
        } else {
            Session::flash('flash_message', 'program is not exist!');
            return redirect('admin/program');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'program_name' => 'required',
            'level' => 'required|in:1,2,3',
            'classification_id' => 'required',
            'category_id' => 'required',
            'duration' => 'required|integer',
            'attachement' => 'sometimes|mimes:zip,rar'
        ]);
        $data = array(
            'program_name' => $request->input('program_name'),
            'classification_id' => $request->input('classification_id'),
            'category_id' => $request->input('category_id'),
            'duration' => $request->input('duration'),
            'level' => $request->input('level')
        );

        if ($request->input('description')) {
            $data['description'] = $request->input('description');
        }
        if ($request->file('attachement')) {
            $fimage = $request->file('attachement');
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('/uploads/program/'), $filename);
            $data['attachement'] = '/uploads/program/' . $filename;
        }

        $program = program::where('id', $id);
        $program->update($data);
        Session::flash('flash_message', 'Program Updated Successfully!');
        return redirect('admin/program');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */

    public function destroy(Request $request, $id)
    {
        $program = program::where('id', $id);
        $program->delete();
        $message = 'Program Deleted';
        return response()->json(['message' => $message], 200);

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request, $id)
    {
        $program = program::where('id', $id)->first();
        if ($program == null) {
            Session::flash('flash_message', 'program is not exist!');
            return redirect('admin/program');
        }
        //change client status
        $status = $request->get('status');
        if ($status != '') {
            if ($status == 0) {
                $program->status = 0;
                $program->update();
                Session::flash('flash_message', 'program Status change to Inactive');
            } else {
                $program->status = 1;
                $program->update();
                Session::flash('flash_message', 'program Status change to Active');
            }

        }
        return redirect('admin/program');
    }


    public function getCategory(Request $request, $id)
    {
        $category = Category::where('classification_id', $id)->where('status', '1')->pluck('cat_name', 'id');
        $category = json_encode($category);
        return $category;
    }

    public function uploadeExcel()
    {
        return view('admin.program.uploadeexcel');
    }

    public function upload(Request $request)
    {   

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 10000000);

        $this->validate($request, [
            'uploadExcel' => 'required|mimes:xlsx,xls,xlsm,xlsb'
        ]);
        if ($request->hasFile('uploadExcel')) {
            $path = $request->file('uploadExcel')->getRealPath();
            $data = \Excel::load($path)->get();   // get sheet collections

            if ($data->count()) {
                
                // remove all the data of Programs, category and Employee-programs before uploading of excel
                Category::truncate();
                Program::truncate();
                EmployeeProgram::truncate();

                foreach ($data as $key => $valueCheck) {
                    
                    $goOn = true;
                    if (is_object($valueCheck)) {
                        foreach ($valueCheck as $dataValue) {
                            if ($dataValue == '') {
                               // $goOn = false;
                            }
                        }
                    } else {
                        $goOn = false;
                    }
                    if ($goOn) {
                        foreach ($valueCheck as $key2 => $value) {
                           
                            $goOnSecond = true;
                            if (is_object($value)) {
                                foreach ($value as $dataValue) {
                                    if ($dataValue == '') {
                                       // $goOnSecond = false;
                                    }
                                }
                            } else {
                                $goOnSecond = false;
                            }
                            if ($goOnSecond) {
                                // get the level
                                $level =  $valueCheck->getTitle();
                                if($level){
                                    if($level == 'L1'){
                                        $level = 1;
                                    }
                                    if ($level == 'L2') {
                                        $level = 2;
                                    }
                                    if ($level == 'L3') {
                                        $level = 3;
                                    }
                                }

                                // classification
                                $calssificationId = 1 ;

                                // get the category
                                $heading = $valueCheck->getHeading();
                                foreach ($heading as $k => $v) {
                                    if ($k < 1) continue;
                                    $cat_name =  ucwords(str_replace("_", " ", $v)); 
                                    // Check Category
                                    $getCategory = Category::where('cat_name', $cat_name)->first();
                                    if ($getCategory) {
                                        $categoryId = $getCategory->id;
                                    } else {
                                        $category = Category::create(array('cat_name' => $cat_name, 'classification_id' => $calssificationId, 'status' => 1));
                                        $categoryId = $category->id;
                                    }
                                    $program_name = $value[$v];
                                    // Check Programe
                                    $getPrograme = Program::where('program_name', $program_name)->where('category_id', $categoryId)->where('classification_id', $calssificationId)->where('level', $level)->first();
                                    if ($getPrograme) {
                                        $programeId = $getPrograme->id;
                                    } else {
                                        if( $program_name != ""){
                                            $data = array(
                                                'program_name' => $program_name,
                                                'classification_id' => $calssificationId,
                                                'category_id' => $categoryId,
                                                'level' => $level,
                                                'status' => 1
                                            );
                                            $programe = Program::create($data);
                                            $programeId = $programe->id;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                Session::flash('flash_message', 'Excel Uploaded Successfully');
            } else {
                Session::flash('flash_message', 'Excel can not be Uploaded');
            }
            return redirect('admin/program');
        }

    }

    public function select(){

        if(Auth::user()->roles[0]->name == "employee" || Auth::user()->roles[0]->name == "manager" ){
            $selected_programs = [];  $select_status = 0;
            $user = Auth::user()->employee ;
            $category = Category::with('program')->get();
            $selected_programs = $user->programs->pluck('id')->toArray();
            $select_status = EmployeeProgram::whereIn('program_id',$selected_programs)->where('employee_id',$user->id)->groupBy('parent_id')->pluck('status')->first();           
            return view('admin.program.select', compact('category','selected_programs','select_status'));
  
        }
        return redirect()->back();
        
    }

    public function program_submit(Request $request){
        if($request->has('id') && $request->get('id') != "" ){
            $user = Auth::user()->employee ;
            $programs = $request->get('id');

            $select_status = EmployeeProgram::where('employee_id',$user->id)->groupBy('parent_id')->pluck('status')->first();   

            if(!$select_status){

                foreach($programs as $program){
                     EmployeeProgram::create([
                        'program_id' => $program,
                        'employee_id' => $user->id,
                        'status' => 3
                    ]);
                }
                $parent_id = EmployeeProgram::whereIn('program_id',$programs)->where('employee_id',$user->id)->where('status', 3)->pluck('id')->first();
                if($parent_id){
                    EmployeeProgram::whereIn('program_id',$programs)->where('employee_id',$user->id)->where('status', 3)->update(['parent_id' => $parent_id ]);
                }

            }

            // if($select_status == 2){

            //     // check with the previous value 
            //     $selected_programs = $user->programs->pluck('id')->toArray();
            //     $programs = array_map('intval', $programs) ;
                
            //     if(!array_diff($selected_programs , $programs )){
            //         Session::flash('flash_message', 'Please Select another Programs, the request for this programs have been cancelled');
            //         return redirect()->back();
            //     }else{

            //         // delete previous record 
            //         EmployeeProgram::where('employee_id',$user->id)->delete();

            //         foreach($programs as $program){
            //             EmployeeProgram::create([
            //                 'program_id' => $program,
            //                 'employee_id' => $user->id
            //             ]);
            //         }
            //         $parent_id = EmployeeProgram::whereIn('program_id',$programs)->where('employee_id',$user->id)->where('status', 1)->pluck('id')->first();
            //         if($parent_id){
            //             EmployeeProgram::whereIn('program_id',$programs)->where('employee_id',$user->id)->where('status', 1)->update(['parent_id' => $parent_id ]);
            //         }

            //     }
            // }            
            Session::flash('flash_message', 'Selected program have been updated Successfully');
        }
        return redirect()->back();
           
    }
}
