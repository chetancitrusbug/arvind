<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Trainer;
use App\Vendor;
use App\Program;
use App\TrainerSpecialization;
use Session;
use Yajra\Datatables\Datatables;

class TrainerController extends Controller
{
    public function index(Request $request)
    {

        return view('admin.trainer.index');
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $trainer = Trainer::select('trainers.*','vendor.vendor_name')
        ->join('vendor','trainers.vendor_id','vendor.id');

        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(trainers.trainer_code LIKE  '%$value%' OR trainers.trainer_name LIKE  '%$value%' OR trainers.email LIKE  '%$value%' OR vendor.vendor_name  LIKE  '%$value%' )";
                $trainer = $trainer->whereRaw($where_filter);
            }
        }
        
        $trainer = $trainer->orderby('trainers.id','desc')->get();

        return Datatables::of($trainer)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {       
        $program = Program::where('status','1')->pluck('program_name','id');
        $vendor = Vendor::where('status','1')->pluck('vendor_name','id')->prepend('Select Vendor','');
        
        return view('admin.trainer.create',compact('vendor','program'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'trainer_name' => 'required',
            'mobile' => 'required|digits_between:10,12',
            'email' => ['required', \Illuminate\Validation\Rule::unique('trainers', 'email')->whereNull('deleted_at')],
            'trainer_code' => ['required', \Illuminate\Validation\Rule::unique('trainers', 'trainer_code')->whereNull('deleted_at')],
            'vendor_id' => 'required',
            'address' => 'required',
            'program' => 'required',
            'status' => 'required',
        ]);
       
        $data = array(
            'trainer_name' => $request->input('trainer_name'),
            'mobile' => $request->input('mobile'),
            'address' => $request->input('address'),
            'vendor_id' => $request->input('vendor_id'),
            'email' => $request->input('email'),
            'trainer_code' => $request->input('trainer_code'),
            'status' => $request->input('status'));

        $trainer = Trainer::where('email', $request->input('email'))->withTrashed()->first();
        if($trainer == null)
        {
            $trainer = Trainer::create($data);
        }
        else
        {   
            $trainer->restore();
            $trainer->update($data); 
            TrainerSpecialization::where('trainer_id',$trainer->id)->where('status','1')->delete();           
        }
        if($trainer != null)
        {
            foreach ($request->program as $key => $value) {
                $trainerData['trainer_id'] = $trainer->id;
                $trainerData['program_id'] = $value;
                TrainerSpecialization::create($trainerData);
            }
        }
        //Welcome Mail Send
        \Mail::send('email.welcome', ['name' => $trainer->trainer_name], function ($message) use ($trainer) {
            $message
            ->to($trainer['email'])
            ->subject('Welcome to Arvind University');                           
        });
        Session::flash('flash_message', 'Trainer added!');
        return redirect('admin/trainer');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $program = Program::where('status','1')->pluck('program_name','id');
        $trainer = Trainer::where('id', $id)->first();
        $vendor = Vendor::where('status','1')->pluck('vendor_name','id')->prepend('Select Vendor','');
        $trainer->program = TrainerSpecialization::where('trainer_id',$id)->where('status','1')->pluck('program_id');
       
        if ($trainer) {
            return view('admin.trainer.edit', compact('trainer','vendor','program'));
        } else {
            Session::flash('flash_message', 'Trainer is not exist!');
            return redirect('admin/trainer');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'trainer_name' => 'required',
            'mobile' => 'required|digits_between:10,12',
            'address' => 'required',
            'vendor_id' => 'required',
            'program' => 'required',
            'status' => 'required',
        ]);
       
        $requestData = array(
            'trainer_name' => $request->input('trainer_name'),
            'mobile' => $request->input('mobile'),
            'address' => $request->input('address'),
            'vendor_id' => $request->input('vendor_id'),
            'status' => $request->input('status'));
        
        $trainer = Trainer::where('id', $id);
        $trainer->update($requestData);

       TrainerSpecialization::where('trainer_id',$id)->where('status','1')->delete();
       foreach ($request->program as $key => $value) {
            $trainerData['trainer_id'] = $id;
            $trainerData['program_id'] = $value;
            TrainerSpecialization::create($trainerData);
        }
        Session::flash('flash_message', 'Trainer Updated Successfully!');
        return redirect('admin/trainer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    
    public function destroy(Request $request, $id)
    {
        $trainer = Trainer::where('id', $id);
        $trainer->delete();
        $message='Trainer Deleted';
        return response()->json(['message'=>$message],200);

    }

   
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {
        $trainer = Trainer::where('id', $id)->first();
        if($trainer == NULL) {
            Session::flash('flash_message', 'trainer is not exist!');
            return redirect('admin/trainer');
        }
        //change client status
        $status = $request->get('status');
        if($status != ''){
            if($status == '0' ){
                $trainer->status= '0';
                $trainer->update();            
                Session::flash('flash_message', 'Trainer Status change to inactive');
            }else{
                $trainer->status= '1';
                $trainer->update();               
                Session::flash('flash_message', 'Trainer Status change to active');
            }

        }
        return redirect('admin/trainer');
    }

    
    
}
