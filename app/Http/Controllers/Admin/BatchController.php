<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Brand;
use App\Location;
use App\Department;
use App\Designation; 
use App\Division;
use App\Batch;
use App\BatchNominee;
use App\Region;
use App\Vendor;
use App\Program;
use App\User;
use App\Trainer;
use App\TrainerSpecialization;
use App\Employee;
use App\Notification;
use Session;
Use Crypt;
use Yajra\Datatables\Datatables;
 
class BatchController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.batch.index');
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $batch = Batch::select('batch.*','trainers.trainer_name','vendor.vendor_name','program.program_name')
        ->join('trainers', 'batch.trainer_id', '=', 'trainers.id')
        ->join('vendor', 'batch.vendor_id', '=', 'vendor.id')
        ->join('program', 'batch.program_id', '=', 'program.id');

        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(batch.id LIKE  '%$value%' OR batch.date LIKE  '%$value%' OR vendor.vendor_name LIKE  '%$value%' OR trainers.trainer_name LIKE  '%$value%' OR program.program_name LIKE  '%$value%'  )";
                $batch = $batch->whereRaw($where_filter);
            }
        }
        $batch = $batch->orderby('batch.id','desc')->get();

        return Datatables::of($batch)
            ->make(true);
        exit;
    }

    // Empolyee List for create batch
    public function employeeTable(Request $request)
    {
        //dd($request->all());
        $rules = array(
            'level' => 'sometimes|in:1,2,3,0',
        );
        
        if($request->date != null)
        {
            $rules['date'] = 'required|date|after:' . date('Y-m-d');
        }

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            return response()->json(['success' => false,'message' => $message]);
        }
        /*if(isset($request->batch_id) && $request->batch_id != null)
        {
            $batch = Batch::where('id', $request->batch_id)->first();
            if(isset($batch) && ($batch->trainer_id != $request->trainer_id))
            {
                $batch = Batch::where('date', $request->date)->where('trainer_id', $request->trainer_id)->first();
                if($batch != null)
                {
                    return response()->json(['success' => false,'message' => 'Trainer are busy in other prgram please select other trainer or date!']);
                }
                $trainerSpecialization = TrainerSpecialization::where('program_id', $request->program_id)->where('trainer_id', $request->trainer_id)->first();
                if($trainerSpecialization == null)
                {
                    return response()->json(['success' => false,'message' => "Trainer doesn't have specialization in this program please select other trainer or program!"]);
                }
            } 
            

        }
        else
        {*/
            if($request->trainer_id != null && $request->date != null)
            {
                $batch = Batch::where('date', $request->date)->where('trainer_id', $request->trainer_id)->first();
                if($batch != null)
                {
                    return response()->json(['success' => false,'message' => 'Trainer are busy in other prgram please select other trainer or date!']);
                }
            }
            if($request->trainer_id != null && $request->program_id != null)
            {
                $trainerSpecialization = TrainerSpecialization::where('program_id', $request->program_id)->where('trainer_id', $request->trainer_id)->first();
                if($trainerSpecialization == null)
                {
                    return response()->json(['success' => false,'message' => "Trainer doesn't have specialization in this program please select other trainer or program!"]);
                }
            }
        
        
        
        $employee = Employee::select('employee.*')->with('department')
        ->join('employee_program', 'employee_program.employee_id', '=', 'employee.id');
        
        if(isset($request->program_id) && $request->program_id != 0)
        {
            $employee = $employee->where('employee_program.program_id', $request->program_id);
        }
        if(isset($request->level) && $request->level != 0)
        {
            $employee = $employee->where('employee.level', $request->level);
        }
        if(isset($request->region_id) && $request->region_id != null)
        {
            $employee = $employee->where('employee.region_id', $request->region_id);
        }
        if(isset($request->location_id) && $request->location_id != null)
        {
            $employee = $employee->where('employee.location_id', $request->location_id);
        }
        if(isset($request->department_id) && $request->department_id != null)
        {
            $employee = $employee->where('employee.department_id', $request->department_id);
        }
        if(isset($request->designation_id) && $request->designation_id != null)
        {
            $employee = $employee->where('employee.designation_id', $request->designation_id);
        }
        if(isset($request->division_id) && $request->division_id != null)
        {
            $employee = $employee->where('employee.division_id', $request->division_id);
        }
        $employee = $employee->distinct()->orderby('employee.id','desc')->get();

        $html = view('admin.batch.employeelist',compact('employee'))->render();
        return response()->json(['success' => true,'data' => $html,'message' => 'Success!']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        $program = Program::where('status','1')->pluck('program_name','id')->prepend('Select Program',''); 
        $vendor = Vendor::where('status','1')->pluck('vendor_name','id')->prepend('Select Vendor',''); 
        $region = Region::where('status','1')->pluck('region_name','id')->prepend('Select Region','');
        $division = Division::where('status','1')->pluck('division_name','id')->prepend('Select Division','');
        $designation = Designation::where('status','1')->pluck('designation_name','id')->prepend('Select Designation','');
        $department = Department::where('status','1')->pluck('department_name','id')->prepend('Select Department','');
        
        $employee = Employee::select('employee.*')->with('department')
            ->join('employee_program', 'employee_program.employee_id', '=', 'employee.id')
            ->where('employee.level', 1)
            ->orderby('employee.id','desc')->distinct()->get();

        return view('admin.batch.create',compact('region','division','designation','department','program','vendor','employee'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
            'idsArr' => 'required',
            'datepicker' => 'required|date',
            'program_id' => 'required',
            'level' => 'required|in:1,2,3,0',
            'vendor_id' => 'required',
            'trainer_id' => 'required',
        ]);
        $data = array(
            'date' => $request->input('datepicker'),
            'program_id' => $request->input('program_id'),
            'vendor_id' => $request->input('vendor_id'),
            'trainer_id' => $request->input('trainer_id'));

        $batch = Batch::create($data);
        $program = Program::where('id', $request->program_id)->first();
        $vendor = Vendor::where('id', $request->vendor_id)->first();
        $trainer = Trainer::where('id', $request->trainer_id)->first();

        $programName = ((isset($program->program_name) ? $program->program_name : ''));
        $vendorName = ((isset($vendor->vendor_name) ? $vendor->vendor_name : ''));
        $trainnerName = ((isset($trainer->trainer_name) ? $trainer->trainer_name : ''));

        $empArr = explode(',',$request->idsArr);
        foreach ($empArr as $key => $value) {
            $empdata['batch_id'] = $batch->id;
            $empdata['employee_id'] = $value;
            $empdata['batch_nominee_token'] = uniqid();
            $batchNominee = BatchNominee::create($empdata);
            $employee = Employee::where('id', $value)->first();

            /*$view = view('email.inviteEmployee',['date' =>$request->datepicker,'programName'=>$programName,'vendorName' => $vendorName , 'trainnerName' => $trainnerName,'batchId' => $empdata['batch_nominee_token']]);
            $html = $view->render();
            echo $html;exit;*/

            //invitation mail

            $data = ['date' =>$request->datepicker,'programName'=>$programName,'vendorName' => $vendorName , 'trainnerName' => $trainnerName,'batchId' => $empdata['batch_nominee_token'],'employee' => $employee];

            /*Mail::to($employee->email)
                ->subject('Invited to Batch')
                ->send(new MailQueue($data));*/

            \Mail::send('email.inviteEmployee', ['date' =>$request->datepicker,'programName'=>$programName,'vendorName' => $vendorName , 'trainnerName' => $trainnerName,'batchId' => $empdata['batch_nominee_token'],'employee' => $employee], function ($message) use ($employee) {
            $message
                ->to($employee->email)
                ->subject('Invited to Batch');                           
            });
        } 
        $adminEmail = config('admin.admin_data.email_id');
        //Welcome Mail Send
        /*$view = view('email.batchTrainer', ['date' =>$request->datepicker,'programName'=>$programName,'vendorName' => $vendorName , 'trainnerName' => $trainnerName,'batchId' => $batch->id]);
        $html = $view->render();
        echo $html;exit;*/
        
        \Mail::send('email.adminApprovalBatch', ['date' =>$request->datepicker,'programName'=>$programName,'vendorName' => $vendorName , 'trainnerName' => $trainnerName,'batchId' => $batch->id], function ($message) use ($adminEmail) {
            $message 
            ->to($adminEmail)
            ->subject('Batch Created');                           
        });
        //Trainer Mail
        \Mail::send('email.batchTrainer', ['date' =>$request->datepicker,'programName'=>$programName,'vendorName' => $vendorName , 'trainnerName' => $trainnerName,'batchId' => $batch->id], function ($message) use ($trainer) {
            $message 
            ->to($trainer->email)
            ->subject('Batch Created');                                      
        });
        //Vendor Mail
        \Mail::send('email.batchVendor', ['date' =>$request->datepicker,'programName'=>$programName,'vendorName' => $vendorName , 'trainnerName' => $trainnerName,'batchId' => $batch->id], function ($message) use ($vendor) {
            $message 
            ->to($vendor->email)
            ->subject('Batch Created');                                      
        });
        Session::flash('flash_message', 'Batch added!');
        return redirect('admin/batch');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    /*public function edit(Request $request, $id)
    {
        $batch = Batch::where('id', $id)->first();
        $program = Program::where('status','1')->pluck('program_name','id')->prepend('Select Program',''); 
        $vendor = Vendor::where('status','1')->pluck('vendor_name','id')->prepend('Select Vendor',''); 
        $region = Region::where('status','1')->pluck('region_name','id')->prepend('Select Region','');
        $division = Division::where('status','1')->pluck('division_name','id')->prepend('Select Division','');
        $designation = Designation::where('status','1')->pluck('designation_name','id')->prepend('Select Designation','');
        $department = Department::where('status','1')->pluck('department_name','id')->prepend('Select Department','');
        
        if ($batch) {
            $batchEmpIds = BatchNominee::where('batch_id', $id)->pluck('employee_id');
            $batchEmpIdsCount = count($batchEmpIds);
            $batchEmpIdsString = implode('|',$batchEmpIds->toArray());
            $employee = Employee::select('employee.*')->with('department')
            ->join('employee_program', 'employee_program.employee_id', '=', 'employee.id')
            ->where('employee_program.program_id', $batch->program_id)
            ->orderby('employee.id','desc')->get();
            $trainer = Trainer::where('vendor_id', $batch->vendor_id)->where('status','1')->pluck('trainer_name','id');
            return view('admin.batch.edit', compact('batchEmpIdsCount','batchEmpIdsString','batchEmpIds','batch','employee','region','division','designation','department','program','vendor','trainer'));
        } else {
            Session::flash('flash_message', 'batch is not exist!');
            return redirect('admin/batch');
        }
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    /*public function update($id, Request $request)
    {
       //dd($request->all());
        $this->validate($request, [
            'idsArr' => 'required',
            'datepicker' => 'required|date',
            'program_id' => 'required',
            'level' => 'required|in:1,2,3,0',
            'vendor_id' => 'required',
            'trainer_id' => 'required',
            'batch_id' => 'required',
        ]);

        $batch = Batch::with('nominee')->where('id', $id)->first();
        $program = Program::where('id', $request->program_id)->first();
        $vendor = Vendor::where('id', $request->vendor_id)->first();
        $trainer = Trainer::where('id', $request->trainer_id)->first();
        $programName = ((isset($program->program_name) ? $program->program_name : ''));
        $vendorName = ((isset($vendor->vendor_name) ? $vendor->vendor_name : ''));
        $trainnerName = ((isset($trainer->trainer_name) ? $trainer->trainer_name : ''));
        if( isset($batch) && $batch->approval == 0)
        {
            $pastProgram = Program::where('id', $batch->program_id)->first();
            $pastProgramName = ((isset($pastProgram->program_name) ? $pastProgram->program_name : ''));
            if($batch->program_id != $request->program_id)
            {
                */
                //Trainer Mail

                /*\Mail::send('email.batchPastTrainer', ['date' =>$batch->date,'programName'=>$pastProgramName,'trainnerName' => $pastTrainnerName ,'batchId' => $batch->id], function ($message) use ($pastTrainer) {
                    $message 
                    ->to($pastTrainer->email)
                    ->subject('Batch Update!');                                      
                });*/
            /*}
            if($batch->trainer_id != $request->trainer_id)
            {
                $pastTrainer = Trainer::where('id', $batch->trainer_id)->first();
                $pastTrainnerName = ((isset($pastTrainer->trainer_name) ? $pastTrainer->trainer_name : ''));
                //Trainer Mail

                /*\Mail::send('email.batchPastTrainer', ['date' =>$batch->date,'programName'=>$pastProgramName,'trainnerName' => $pastTrainnerName ,'batchId' => $batch->id], function ($message) use ($pastTrainer) {
                    $message 
                    ->to($pastTrainer->email)
                    ->subject('Batch Update!');                                      
                });*/

                /*\Mail::send('email.batchTrainer', ['date' =>$request->datepicker,'programName'=>$programName,'vendorName' => $vendorName , 'trainnerName' => $trainnerName,'batchId' => $batch->id], function ($message) use ($trainer) {
                    $message 
                    ->to($trainer->email)
                    ->subject('Batch Created');                                      
                });*/
                /*
            } 
            if($batch->vendor_id != $request->vendor_id)
            {
                $pastVendor = Vendor::where('id', $batch->vendor_id)->first();
                $pastVendorName = ((isset($pastVendor->vendor_name) ? $pastVendor->vendor_name : ''));
                //Vendor Mail
                /*\Mail::send('email.batchPastVendor', ['date' =>$batch->date,'programName'=>$pastProgramName,'vendorName' => $pastVendorName ,'batchId' => $batch->id], function ($message) use ($pastVendor) {
                    $message 
                    ->to($pastVendor->email)
                    ->subject('Batch Created');                                      
                });*/

                /*\Mail::send('email.batchVendor', ['date' =>$request->datepicker,'programName'=>$programName,'vendorName' => $vendorName , 'trainnerName' => $trainnerName,'batchId' => $batch->id], function ($message) use ($vendor) {
                    $message 
                    ->to($vendor->email)
                    ->subject('Batch Created');                                      
                });*/
           /* }
            $requestData = array(
                'date' => $request->input('datepicker'),
                'program_id' => $request->input('program_id'),
                'vendor_id' => $request->input('vendor_id'),
                'trainer_id' => $request->input('trainer_id'));
            
            $batch->update($requestData);
        }
        else
        {
            Session::flash('flash_message', "Batch is approved you can't updated");
            return redirect('admin/batch');
        }
        
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    
    public function destroy(Request $request, $id)
    {
        $batch = Batch::where('id', $id)->first();
        $batchNominee = BatchNominee::where('batch_id', $id)->get();

        $program = Program::where('id', $batch->program_id)->first();
        $vendor = Vendor::where('id', $batch->vendor_id)->first();
        $trainer = Trainer::where('id', $batch->trainer_id)->first();
        $programName = ((isset($program->program_name) ? $program->program_name : ''));
        $vendorName = ((isset($vendor->vendor_name) ? $vendor->vendor_name : ''));
        $trainnerName = ((isset($trainer->trainer_name) ? $trainer->trainer_name : ''));

        foreach ($batchNominee as $key => $value) {
            $employee = Employee::where('id', $value['employee_id'])->first();
            if(isset($employee))
            {
                // Emp Mail
                \Mail::send('email.batchCancelled', ['date' =>$batch->date,'programName'=>$programName,'vendorName' => $vendorName ,'trainnerName' => $trainnerName ,'batchId' => $batch->id,'userName' => $employee->emp_name], function ($message) use ($employee) {
                    $message 
                    ->to($employee->email)
                    ->subject('Batch Cancelled!');                                      
                });
            }            
        }
        if(isset($vendor))
        {
            // Vendor Mail
            \Mail::send('email.batchCancelled', ['date' =>$batch->date,'programName'=>$programName,'vendorName' => $vendorName ,'trainnerName' => $trainnerName ,'batchId' => $batch->id,'userName' => $vendor->vendor_name], function ($message) use ($vendor) {
                $message 
                ->to($vendor->email)
                ->subject('Batch Cancelled!');                                      
            });
        }
        
        if(isset($trainer))
        {
            // Trainer Mail
            \Mail::send('email.batchCancelled', ['date' =>$batch->date,'programName'=>$programName,'vendorName' => $vendorName ,'trainnerName' => $trainnerName ,'batchId' => $batch->id,'userName' => $trainer->trainer_name], function ($message) use ($trainer) {
                $message 
                ->to($trainer->email)
                ->subject('Batch Cancelled!');                                      
            });
        }

        $batch = $batch->delete();
        $batchNominee = BatchNominee::where('batch_id', $id)->delete();
        $message='Batch Deleted';
        return response()->json(['message'=>$message],200);

    }

   
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {
        $batch = Batch::with('nominee_accepted','nominee_rejected','nominee','program','vendor','trainer','user')->where('id', $id)->first();
        //dd($batch);
        
        if($batch == NULL) {
            Session::flash('flash_message', 'Batch is not exist!');
            return redirect('admin/batch');
        }
        
        //change client status
        $status = $request->get('status');
        $approval = $request->get('approval');
        if($approval != ''){
            if($approval == 1 ){
                $batch->approval= 1;
                $batch->update();            
                Session::flash('flash_message', 'Batch Approved!');
                return redirect('admin/batch');
            }
        }
        if($status != ''){
            if($status == 0 ){
                $batch->status= 0;
                $batch->update();            
                Session::flash('flash_message', 'batch Status change to Inactive');
                return redirect('admin/batch');
            }else{
                $batch->status= 1;
                $batch->update();               
                Session::flash('flash_message', 'batch Status change to Active');
                return redirect('admin/batch');
            }
         
        }
        $batch->batch_password =  (isset($batch->batch_password) && $batch->batch_password != null ? Crypt::decrypt($batch->batch_password) : '');
        //dd($batch);
        return view('admin.batch.show',compact('batch'));
    }

    
    public function getTrainer(Request $request,$id)
    {
        $trainer = Trainer::join('trainer_specialization','trainers.id','trainer_specialization.trainer_id')
        ->where('trainer_specialization.program_id', $request->program_id)
        ->where('trainers.vendor_id', $id)
        ->where('trainers.status','1')
        ->pluck('trainers.trainer_name','trainers.id'); 
        $trainer = json_encode($trainer);
        return $trainer;
    }

    public function batchApproval(Request $request,$approval,$id)
    {
        $batch = Batch::with('nominee','program','vendor','trainer')->where('id', $request->id)->first();
        if($batch == NULL) {
            Session::flash('flash_message', 'Batch is not exist!');
            return redirect('admin/batch');
        }
        elseif($batch->approval == 1)
        {
            Session::flash('flash_message', 'Batch is already Approved!');
            return redirect('admin/batch');
        }

        $pwd = rand(000000,999999); 
        $pwdCrypt = Crypt::encrypt($pwd);
        if($approval == 1)
        $requestData['batch_password'] = $pwdCrypt;

        $requestData['approval'] = $approval;
        $batch->update($requestData);

        $programName = ((isset($batch->program->program_name) ? $batch->program->program_name : ''));
        $vendorName = ((isset($batch->vendor->vendor_name) ? $batch->vendor->vendor_name : ''));
        $trainnerName = ((isset($batch->trainer->trainer_name) ? $batch->trainer->trainer_name : ''));

        if($approval == 1)
        {
            $message='Batch Approved Succesfully!';

            
            $data['email'] = 'batch.'.$batch->id.'@gmail.com';
            $data['password'] = bcrypt($pwd);
            $data['status'] = 1;
            $data['batch_id'] = $batch->id;
            $user = User::create($data);

            $user->assignRole('batch');

            if(isset($batch->vendor))
            {
                // Vendor Mail
                \Mail::send('email.batchLoginDetails', ['email'=>$data['email'],'password'=>$pwd,'date' =>$batch->date,'programName'=>$programName,'vendorName' => $vendorName ,'trainnerName' => $trainnerName ,'batchId' => $batch->id,'userName' => $batch->vendor->vendor_name], function ($message) use ($batch) {
                    $message 
                    ->to($batch->vendor->email)
                    ->subject('Batch Login Informatiom');                                      
                });
            }
            
            if(isset($batch->trainer))
            {
                // Trainer Mail
                \Mail::send('email.batchLoginDetails', ['email'=>$data['email'],'password'=>$pwd,'date' =>$batch->date,'programName'=>$programName,'vendorName' => $vendorName ,'trainnerName' => $trainnerName ,'batchId' => $batch->id,'userName' => $batch->trainer->trainer_name], function ($message) use ($batch) {
                    $message 
                    ->to($batch->trainer->email)
                    ->subject('Batch Login Informatiom');  
                });
            }

        }
        else{
            $message='Batch Cancelled Succesfully!';
            

            foreach ($batch->nominee as $key => $value) {
                $employee = Employee::where('id', $value['employee_id'])->first();
                if(isset($employee))
                {
                    // Emp Mail
                    \Mail::send('email.batchCancelled', ['date' =>$batch->date,'programName'=>$programName,'vendorName' => $vendorName ,'trainnerName' => $trainnerName ,'batchId' => $batch->id,'userName' => $employee->emp_name], function ($message) use ($employee) {
                        $message 
                        ->to($employee->email)
                        ->subject('Batch Cancelled!');                                      
                    });
                }            
            }
            if(isset($batch->vendor))
            {
                // Vendor Mail
                \Mail::send('email.batchCancelled', ['date' =>$batch->date,'programName'=>$programName,'vendorName' => $vendorName ,'trainnerName' => $trainnerName ,'batchId' => $batch->id,'userName' => $batch->vendor->vendor_name], function ($message) use ($batch) {
                    $message 
                    ->to($batch->vendor->email)
                    ->subject('Batch Cancelled!');                                      
                });
            }
            
            if(isset($batch->trainer))
            {
                // Trainer Mail
                \Mail::send('email.batchCancelled', ['date' =>$batch->date,'programName'=>$programName,'vendorName' => $vendorName ,'trainnerName' => $trainnerName ,'batchId' => $batch->id,'userName' => $batch->trainer->trainer_name], function ($message) use ($batch) {
                    $message 
                    ->to($batch->trainer->email)
                    ->subject('Batch Cancelled!');                                      
                });
            }
        }
        
        Session::flash('flash_message', $message);
        return redirect('admin/batch');

    }

    public function batchInvitation(Request $request)
    {
        $batchNominee = BatchNominee::where('batch_nominee_token', $request->batchId)->first();
        if($batchNominee == NULL) {
            return view('admin.batch.successorfail',['msg' => 'Link has been expired!' ]);
            /*$html = $view->render();
            echo $html;exit;*/
        }
        if($request->email_status == '1')
        {
            $requestData['email_status'] = $request->email_status;
            $message='Thank you for accept invitation!';
        }
        elseif($request->email_status == '2')
        {
            $requestData['email_status'] = $request->email_status;
            $message='Thank you for response!';
        }
        else{
            return view('admin.batch.successorfail',['msg' => 'Batch email status is not exist!' ]);
        }
        $requestData['batch_nominee_token'] = '';
        $batchNominee->update($requestData);
        
        return view('admin.batch.successorfail',['msg' => $message ]);

    }

    public function notificationRead( Request $request,$id)
    {
        $notification = Notification::where('id', $id)->first();
        if($notification == NULL) {
            $message='Notification is not exist!';
            Session::flash('flash_message', $message);
            return redirect('admin/batch');
        }
        $notification->update(['read_status' => 1]);

        $message='Mark as Read!';
        Session::flash('flash_message', $message);
        return redirect('admin/batch'.'/'.$notification->batch_id);

    }

}
