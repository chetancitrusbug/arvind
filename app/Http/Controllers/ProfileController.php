<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Location;
use Hash;

class ProfileController extends Controller
{
    //
    public function index(){

        $user = User::with('location','casino')->find(Auth::user()->id);
        $location = Location::where('status',1)->pluck('name', 'id')->Prepend('Select Location',''); 

        return view('profile.index',compact('user','location'));
    }

    public function change_password(Request $request)
    {
        $message = "";
        $code = 200;
        $cur_password = $request->password;
        $user = Auth::user();

        if (Hash::check($cur_password, $user->password) ) {

            $user->password = Hash::make($request->new_password);
            $user->save();
            $message = "Password Changed Successfully !!" ;

        } else {
            $message = "Please Enter Correct Current Password !!" ;  
            $code = 400 ;
        }
        return response()->json(array( 'message' => $message), $code);

    }

    public function change_location(Request $request)
    {
        $message = "";
        $code = 200;
        $user = Auth::user();

        if ($request->location_id != '' ) {

            $user->location_id = $request->location_id;
            $user->save();
            $message = "Location Set Successfully !!" ;

        } else {
            $message = "Please Select Correct Location !!" ;  
            $code = 400 ;
        }
        return response()->json(array( 'message' => $message), $code);

    }
}
