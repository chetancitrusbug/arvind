<?php

namespace App\Http\Controllers\batch;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Batch;
use App\BatchNominee;
use App\Vendor;
use App\Program;
use App\Trainer;
use App\Employee;
use Session;
use Auth;
Use Crypt;
use Yajra\Datatables\Datatables;


class BatchController extends Controller
{
    public function index(Request $request)
    {
        $id = Auth::user()->batch_id;
        
        $batch = Batch::where('id', $id)->first();
        if($batch == null)
        {
            Session::flash('flash_message', 'Batch not exist!');
            return redirect('batchuser/batch');
        }

        $program = Program::where('id', $batch->program_id)->first();
        $vendor = Vendor::where('id', $batch->vendor_id)->first();
        $trainer = Trainer::where('id', $batch->trainer_id)->first();
        $batch->programName = ((isset($program->program_name) ? $program->program_name : ''));
        $batch->vendorName = ((isset($vendor->vendor_name) ? $vendor->vendor_name : ''));
        $batch->trainnerName = ((isset($trainer->trainer_name) ? $trainer->trainer_name : ''));
        return view('batch.batch.index',compact('batch'));
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $id = Auth::user()->batch_id;
        $batchNominee = BatchNominee::select('batch_nominee.*','employee.*')
        ->join('employee', 'batch_nominee.employee_id', '=', 'employee.id')
        ->where('batch_nominee.batch_id', $id)
        ->where('batch_nominee.email_status', 1);
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(employee.emp_code LIKE  '%$value%' OR employee.emp_name LIKE  '%$value%' OR employee.email LIKE  '%$value%'  )";
                $batchNominee = $batchNominee->whereRaw($where_filter);
            }
        }
        $batchNominee = $batchNominee->orderby('batch_nominee.id','desc')->get();

        return Datatables::of($batchNominee)
            ->make(true);
        exit;
    }

    public function attendence( Request $request)
    {
        $batchNominee = BatchNominee::where('employee_id', $request->empid)->where('batch_id', $request->batchId)->first();
        if($batchNominee == NULL) {
            $message='Batch or employee is not exist!';
            return response()->json(['message'=>$message],200);
        }
        $batchNominee->update(['attendence_status' => $request->action ]);

        $message='Action Update!';
        return response()->json(['message'=>$message],200);

    }

    public function sendFeedbackForm( Request $request)
    {
        $batch = Batch::with('program','vendor','trainer')->where('id',$request->batchId)->first();
        
        $batchNominee = BatchNominee::where('batch_id', $request->batchId)->where('attendence_status',1)->get();

        if(count($batchNominee) < 1) {
            $message='Attendence status is zero!';
            return response()->json(['message'=>$message],200);
        }
        $programName = ((isset($batch->program->program_name) ? $batch->program->program_name : ''));
        $vendorName = ((isset($batch->vendor->vendor_name) ? $batch->vendor->vendor_name : ''));
        $trainnerName = ((isset($batch->trainer->trainer_name) ? $batch->trainer->trainer_name : ''));

        foreach ($batchNominee as $key => $value) {
            //invitation mail
            $employee = Employee::where('id', $value->employee_id)->first();
            if($employee != '')
            {
                $link = url('batchfeedback').'/'.Crypt::encrypt($batch->id);
                \Mail::send('email.batchFeedbackLink', ['date' =>$batch->date,'programName'=>$programName,'vendorName' => $vendorName , 'trainnerName' => $trainnerName,'userName' => $employee->emp_name , 'link' => $link], function ($message) use ($employee) {
                $message
                    ->to($employee->email)
                    ->subject('Feedback to Batch');                           
                });

            }
            
        } 
        $message='Sended Successfully!';
        return response()->json(['message'=>$message],200);

    }
    public function addEmployee( Request $request)
    {
        $employee = Employee::where('emp_code', $request->emp_code)->first();
        if($employee == null)
        {
            $message='Employee Code does not match!';
            return response()->json(['message'=>$message],200);
        }
        $batchNominee = BatchNominee::where('employee_id', $employee->id)->where('batch_id', $request->batchId)->first();
        if($batchNominee != NULL) {
            $message='Employee is already exist!';
            return response()->json(['message'=>$message],200);
        }

        $empdata['batch_id'] = $request->batchId;
        $empdata['employee_id'] = $employee->id;
        $empdata['email_status'] = 1;
        $empdata['batch_nominee_token'] = uniqid();
        $batchNominee = BatchNominee::create($empdata);

        $message='Employee Added!';
        return response()->json(['message'=>$message],200);

    }
    
    
     
    
}   
