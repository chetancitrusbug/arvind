<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use App\Batch;
use App\Employee;

class ReminderMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ReminderMail:reminderMail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminder Mail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $Enddate = date('Y-m-d', strtotime(date('Y-m-d'). ' + 3 days'));
        $batches = Batch::with('nominee_no_response','nominee_accepted','program','vendor','trainer')->where('date',$Enddate)->where('status',1)->get();
        foreach ($batches as $key => $batch) {
            $programName = ((isset($batch->program->program_name) ? $batch->program->program_name : ''));
            $vendorName = ((isset($batch->vendor->vendor_name) ? $batch->vendor->vendor_name : ''));
            $trainnerName = ((isset($batch->trainer->trainer_name) ? $batch->trainer->trainer_name : ''));
    
            foreach ($batch->nominee_accepted as $key => $value) {
                
                $employee = Employee::where('id', $value['employee_id'])->first();
                if(isset($employee))
                {
                    // Emp Mail
                    \Mail::send('email.batchReminder', ['date' =>$batch->date,'programName'=>$programName,'vendorName' => $vendorName ,'trainnerName' => $trainnerName ,'batchId' => $batch->id,'userName' => $employee->emp_name], function ($message) use ($employee) {
                        $message 
                        ->to($employee->email)
                        ->subject('Batch Reminder!');                                      
                    });
                }            
            }

            foreach ($batch->nominee_no_response as $key => $value) {
                
                $empdata['batch_nominee_token'] = uniqid();
                $value->update($empdata);
                $employee = Employee::where('id', $value['employee_id'])->first();
                if(isset($employee))
                {
                    //invitation mail
                    \Mail::send('email.inviteEmployee', ['date' =>$batch->date,'programName'=>$programName,'vendorName' => $vendorName , 'trainnerName' => $trainnerName,'batchId' => $empdata['batch_nominee_token'],'employee' => $employee], function ($message) use ($employee) {
                    $message
                        ->to($employee->email)
                        ->subject('Invited to Batch');                           
                    });
                }            
            }

            if(isset($batch->vendor))
            {
                // Vendor Mail
                \Mail::send('email.batchReminder', ['date' =>$batch->date,'programName'=>$programName,'vendorName' => $vendorName ,'trainnerName' => $trainnerName ,'batchId' => $batch->id,'userName' => $batch->vendor->vendor_name], function ($message) use ($batch) {
                    $message 
                    ->to($batch->vendor->email)
                    ->subject('Batch Reminder!');                                      
                });
            }
            
            if(isset($batch->trainer))
            {
                // Trainer Mail
                \Mail::send('email.batchReminder', ['date' =>$batch->date,'programName'=>$programName,'vendorName' => $vendorName ,'trainnerName' => $trainnerName ,'batchId' => $batch->id,'userName' => $batch->trainer->trainer_name], function ($message) use ($batch) {
                    $message 
                    ->to($batch->trainer->email)
                    ->subject('Batch Reminder!');                                      
                });
            }
        }
    }
}
