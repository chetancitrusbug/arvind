<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use App\Batch;
use App\Employee;
use App\Notification;

class AdminReminderMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AdminReminderMail:adminReminderMail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Admin Reminder Mail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $Enddate = date('Y-m-d', strtotime(date('Y-m-d'). ' + 2 days'));
        $batches = Batch::with('program','vendor','trainer')->where('date',$Enddate)->where('status',1)->get();
        foreach ($batches as $key => $batch) {
            $notification = Notification::where('batch_id',$batch->id)->first();
            if($notification == null)
            {
                $data['batch_id'] = $batch->id;
                Notification::create($data);
            }
            $programName = ((isset($batch->program->program_name) ? $batch->program->program_name : ''));
            $vendorName = ((isset($batch->vendor->vendor_name) ? $batch->vendor->vendor_name : ''));
            $trainnerName = ((isset($batch->trainer->trainer_name) ? $batch->trainer->trainer_name : ''));
            $adminEmail = config('admin.admin_data.email_id');
            // Admin Reminder
            \Mail::send('email.batchReminder', ['date' =>$batch->date,'programName'=>$programName,'vendorName' => $vendorName ,'trainnerName' => $trainnerName ,'batchId' => $batch->id,'userName' =>'Admin'], function ($message) use ($adminEmail) {
                $message 
                ->to($adminEmail)
                ->subject('Batch Reminder');                           
            });

        }
    }
}
