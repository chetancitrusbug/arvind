<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use App\BatchNominee;

class InvitationClose extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'InvitationClose:invitationClose';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Invitation Close';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $Enddate = date('Y-m-d', strtotime(date('Y-m-d'). ' - 3 days'));
        $batchNominee = BatchNominee::with('employee')->whereDate('created_at',$Enddate)->where('email_status',0)->update(['batch_nominee_token' => '']);
    }
}
