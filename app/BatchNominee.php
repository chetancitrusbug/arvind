<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BatchNominee extends Model
{
    use SoftDeletes;
    protected $table = 'batch_nominee';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'batch_id','employee_id', 'email_status','attendence_status','status','batch_nominee_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    protected $dates = ['deleted_at'];

    public function employee(){
        return $this->belongsTo('App\Employee','employee_id','id');
    }
}