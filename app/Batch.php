<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Batch extends Model
{
    use SoftDeletes;
    protected $table = 'batch';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date','vendor_id', 'status','trainer_id','program_id','approval','batch_password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    protected $dates = ['deleted_at'];

    public function nominee(){
        return $this->hasMany('App\BatchNominee','batch_id','id','employee_id','employee_id');
    }
    public function nominee_accepted(){
        return $this->hasMany('App\BatchNominee','batch_id','id','employee_id','employee_id')->where('email_status','1');
    }
    public function nominee_rejected(){
        return $this->hasMany('App\BatchNominee','batch_id','id','employee_id','employee_id')->where('email_status','2');
    }
    public function nominee_no_response(){
        return $this->hasMany('App\BatchNominee','batch_id','id','employee_id','employee_id')->where('email_status','0');
    }
    public function program(){
        return $this->belongsTo('App\Program','program_id','id');
    }
    public function vendor(){
        return $this->belongsTo('App\Vendor','vendor_id','id');
    }
    public function trainer(){
        return $this->belongsTo('App\Trainer','trainer_id','id');
    }
    public function user(){
        return $this->belongsTo('App\User','id','batch_id');
    }
}
