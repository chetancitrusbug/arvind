<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;
    protected $table = 'employee';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'emp_code', 'emp_name', 'email', 'reporting_manager', 'mobile_no', 'division_id', 'location_id', 'region_id', 'department_id', 'designation_id', 'brand_id', 'status','level'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    protected $dates = ['deleted_at'];

    public function department(){
        return $this->belongsTo('App\Department','department_id','id');
    }

    public function programs(){
        return $this->belongsToMany('App\Program', 'employee_program', 'employee_id','program_id')->whereNull('employee_program.deleted_at');
    }

    public function emp_programs(){       
        return $this->hasMany('App\EmployeeProgram','employee_id','id');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

}
