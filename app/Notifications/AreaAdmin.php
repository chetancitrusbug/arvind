<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AreaAdmin extends Notification
{
    use Queueable;

    protected $area;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($area)
    {
        //
        $this->area = $area;
        
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->subject('Table Gambler - New Area Request')
        ->greeting('Hello, ' . $notifiable->name)
        ->line( $this->area->user->name.' has requested to Add New Area')
        ->action('Please click to Approve Request ', url('/admin/area'))
        ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
