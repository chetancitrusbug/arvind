<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CasinoUser extends Notification
{
    use Queueable;

    protected $casino ;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($casino)
    {
        //
        $this->casino = $casino;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->subject('Table Gambler - New Casino Request')
        ->greeting('Hello, ' . $notifiable->name)
        ->line('Your Request to Add New Casino - '.$this->casino->casino->name.' has been')
        ->line(($this->casino->status == 0 && $this->casino->status != 1 &&  $this->casino->status != 3 && $this->casino->status != 2  ) ? 'Approved' : 'Cancelled'.' by Admin')
        ->action('Click here to See', url('/'))
        ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
