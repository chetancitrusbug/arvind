<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('login-me/{id}', function($id){
    Auth::loginUsingId($id);
    return back();
});

Auth::routes();

Route::get('/', 'Auth\LoginController@showLoginForm')->name('home'); 

Route::group(['middleware' => ['auth', 'admin']], function () {

   // Route::get('/home', 'HomeController@redirect');

    Route::group(['prefix' => 'admin' ], function () {

        Route::get('/', 'Admin\AdminController@index');
        
        //Role & Permissions
        Route::get('/give-role-permissions', 'Admin\AdminController@getGiveRolePermissions');
        Route::post('/give-role-permissions', 'Admin\AdminController@postGiveRolePermissions');
        Route::resource('/roles', 'Admin\RolesController');
        Route::resource('/permissions', 'Admin\PermissionsController');

        //Users
        Route::resource('/users', 'Admin\UsersController');
        Route::get('/users-data', 'Admin\UsersController@datatable');

        //employee
        Route::get('/employee/datatable', 'Admin\EmployeeController@datatable');
        Route::get('/employee/upload-excel', 'Admin\EmployeeController@uploadeExcel')->name('employee.upload');
        Route::post('/employee/upload', 'Admin\EmployeeController@upload')->name('employee.upload.submit');
        Route::get('/getLocation/{id}', 'Admin\EmployeeController@getLocation');
        Route::get('/employee/{id}/programs/', 'Admin\EmployeeController@employeePrograms');
        Route::get('/employee/programs/{id}', 'Admin\EmployeeController@listProgram');
        Route::get('/employee/listProgramDatatable/{id}', 'Admin\EmployeeController@listProgramDatatable');
        Route::get('/employee/batch/{id}', 'Admin\EmployeeController@listBatch');
        Route::get('/employee/listBatchDatatable/{id}', 'Admin\EmployeeController@listBatchDatatable');
        Route::resource('/employee', 'Admin\EmployeeController');
        
        
        //vendor
        Route::get('/vendor/datatable', 'Admin\VendorController@datatable');
        Route::get('vendor/trainer/{id}', 'Admin\VendorController@listTrainer');
        Route::get('/vendor/listTrainerDatatable/{id}', 'Admin\VendorController@listTrainerDatatable');
        Route::resource('/vendor', 'Admin\VendorController');
        

        //trainer
        Route::get('/trainer/datatable', 'Admin\TrainerController@datatable');
        Route::resource('/trainer', 'Admin\TrainerController');

        //program
        Route::get('/program/datatable', 'Admin\ProgramController@datatable');
        Route::get('/program/upload-excel', 'Admin\ProgramController@uploadeExcel')->name('program.upload');
        Route::post('/program/upload', 'Admin\ProgramController@upload')->name('program.upload.submit');
        Route::get('/program-select', 'Admin\ProgramController@select')->name('program.select'); 
        Route::post('/program-select/submit', 'Admin\ProgramController@program_submit')->name('program.select.submit');
        Route::get('/getCategory/{id}', 'Admin\ProgramController@getCategory');
        Route::resource('/program', 'Admin\ProgramController');

        //batch
        Route::get('/batch/datatable', 'Admin\BatchController@datatable');
        Route::get('/batch/employeeTable', 'Admin\BatchController@employeeTable');
        Route::get('/batch/approval/{approval}/{id}', 'Admin\BatchController@batchApproval');
        Route::get('/getTrainers/{id}', 'Admin\BatchController@getTrainer');
        Route::resource('/batch', 'Admin\BatchController');
        
        //notification
        Route::get('/batch/notification/{id}', 'Admin\BatchController@notificationRead');

        //Profile
        Route::get('/profile', 'Admin\ProfileController@index')->name('profile.index');
        Route::get('/profile/edit', 'Admin\ProfileController@edit')->name('profile.edit');
        Route::patch('/profile/edit', 'Admin\ProfileController@update');
        Route::get('/profile/change-password', 'Admin\ProfileController@changePassword')->name('profile.password');
        Route::patch('/profile/change-password', 'Admin\ProfileController@updatePassword');
    });
    
    
});
Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'batchuser' ], function () {
        //batch
        Route::get('/batch/datatable', 'Batch\BatchController@datatable');
        Route::post('/batch/attendence', 'Batch\BatchController@attendence');
        Route::post('/batch/addEmployee', 'Batch\BatchController@addEmployee');
        Route::post('/batch/sendFeedbackForm', 'Batch\BatchController@sendFeedbackForm');
        Route::get('/batch', 'Batch\BatchController@index');
        
    });
});


//invitation accept or reject
Route::get('batch/invitation', 'Admin\BatchController@batchInvitation');


// Feedback
Route::post('/batchfeedback/feedbackStore', 'Batch\FeedbackController@feedbackStore');
Route::get('/batchfeedback/getEmployee', 'Batch\FeedbackController@getEmployee');
Route::post('/batchfeedback/feedbackForm', 'Batch\FeedbackController@feedbackForm');
Route::get('/batchfeedback/{id}', 'Batch\FeedbackController@viewFeedback');


Route::get('/db',function(){
    try {
        DB::connection()->getPdo();
        if(DB::connection()->getDatabaseName()){
            echo "Yes! Successfully connected to the DB: " . DB::connection()->getDatabaseName();
        }
    } catch (\Exception $e) {
        die("Could not connect to the database.  Please check your configuration.");
    }
});


Route::get('/invitationClose', function()
{	
   Artisan::call('InvitationClose:invitationClose');
});
Route::get('/reminderMail', function()
{	
   Artisan::call('ReminderMail:reminderMail');
});
Route::get('/adminReminderMail', function()
{	
   Artisan::call('AdminReminderMail:adminReminderMail');
});
