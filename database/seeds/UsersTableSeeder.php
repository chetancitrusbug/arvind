<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        $user = User::create(
            [
                'name' => 'Admin',
                'email' => 'user.citrusbug@gmail.com',
                'password' => bcrypt(123456),
                'phone' => '',
                // '_website_id' => 1,
                // 'api_token' => str_random(60)
				'status' => 1
            ]
        );


        if (!$user->hasRole('SU')) {
            $user->assignRole('SU');

        }
    }
}
