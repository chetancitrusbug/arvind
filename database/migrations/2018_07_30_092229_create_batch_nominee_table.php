<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchNomineeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batch_nominee', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('batch_id');
            $table->integer('employee_id');
            $table->integer('email_status')->default(0)->comment('1 - Yes ,2 - No , 0 - No Response');
            $table->integer('attendence_status')->default(0)->comment('1 - Yes , 0 - No');
            $table->integer('status')->default(1);
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batch_nominee');
    }
}
