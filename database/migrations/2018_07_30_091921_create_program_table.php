<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program', function (Blueprint $table) {
            $table->increments('id');
            $table->string('program_name')->nullable();
            $table->integer('category_id')->nullable();
            $table->text('description')->nullable();
            $table->integer('classification_id')->nullable();
            $table->enum('level',[1,2,3])->nullable();
            $table->string('attachement')->nullable();
            $table->integer('duration')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program');
    }
}
