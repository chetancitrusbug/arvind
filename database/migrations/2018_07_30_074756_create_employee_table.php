<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->increments('id');
            $table->string('emp_code')->unique();
            $table->string('emp_name')->nullable();
            $table->string('email')->unique();
            $table->string('mobile_no')->nullable();
            $table->string('reporting_manager')->nullable();
            $table->integer('division_id')->nullable();
            $table->integer('location_id')->nullable();
            $table->integer('region_id')->nullable();
            $table->integer('department_id')->nullable();
            $table->integer('designation_id')->nullable();
            $table->integer('brand_id')->nullable();
            $table->enum('level',[1,2,3])->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
