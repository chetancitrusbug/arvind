<!-- Change Location Modal -->
<div id="locModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> @if($user->location) Change @else Add @endif Location</h4>
        </div>
        <div class="modal-body">
          <form id="loc_form" name="loc_form" > 
              <label>Location :</label>
                @if($user->location){{ $user->location->name }}@endif

                {!! Form::select('location_id', $location, isset($user->location) ? $user->location_id : '' , ['class' => 'selectpicker input-95 mb15','data-live-search' => 'true','required']) !!} 

              <button  name="submit" class="btn btn-default">@if($user->location) Change @else Add  @endif</button>  
          </form>
        </div>
      </div>
    </div>
</div>