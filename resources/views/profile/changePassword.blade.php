<!-- Change Password Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Change Password : </h4>
        </div>
        <div class="modal-body">
          <form id="pass_form" name="pass_form" > 
              <label>Current Password</label>
              <input type="password" name="password" id="old_password" /><br>
              <p id="opwd_error"></p>
              <label>New Password</label>
              <input type="password" name="new_password" id="new_password" /><br>
              <p id="npwd_error"></p>
              <label>Confirm Password</label>
              <input type="password" name="confirm_password" id="confirm_password" /><br>
              <p id="cpwd_error"></p>
              <button  name="submit" class="btn btn-default">Change</button>  
          </form>
        </div>
      </div>
    </div>
</div>