<div class="top-header">
	
	<div class="container-fluid">
	<div class="row">
	<div class="col-md-12 col-sm-12">
		
		<div class="logo-div">        	
            <div class="logo-box">
            	<span class="logo-txt"><a href="{{ url('/') }}"><img src="{{ asset('frontend/images/logo.png') }}" class="img-responsive" alt=""></a></span>
            </div>
        </div><!-- end of logo div -->	
		<div class="top-right">
        @guest
            <div class="top-txt-2">
            	<a href="{{ route('login') }}">Login</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="{{ route('register') }}">Register</a>
            </div>
        
        @else
			<div class="top-txt-1">
            	<a href="{{ url('/casino') }}">Add New Casino</a>
            </div>
            <div class="top-txt-2">
            	<span>{{ \Auth::user()->name }}</span>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="{{ url('/profile') }}">User Profile</a>&nbsp;&nbsp;|&nbsp;&nbsp; <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            </div>
      
        @endguest
            
		</div><!-- end of top right -->
		
	</div>
	</div>	
	</div>
</div><!-- end of top header -->