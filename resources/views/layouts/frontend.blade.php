<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7" lang="en-us"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8" lang="en-us"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie10 lt-ie9" lang="en-us"> <![endif]-->
<!--[if IE 9]> <html class="no-js lt-ie10 lt-ie9" lang="en-us"> <![endif]-->
<!--[if lt IE 10]> <html class="no-js lt-ie10" lang="en-us"> <![endif]-->
<!--[if !IE]> > <![endif]-->
<html class='no-js' lang='en'>
<!-- <![endif] -->
<head>
<meta name="description" content="" />
<meta name="author" content="" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<title>@yield('title') {{ config('app.name') }}</title>

<meta content='initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width' name='viewport' />
<meta content='yes' name='apple-mobile-web-app-capable'>
<meta content='translucent-black' name='apple-mobile-web-app-status-bar-style'>
<link href="{{ asset('frontend/images/favicon.png') }}" rel='shortcut icon'>
<link href="{{ asset('frontend/images/favicon.ico') }}" rel='icon' type='image/ico'>

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>

<link href="{{ asset('frontend/css/style.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('frontend/css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('frontend/css/font-awesome.min.css') }}" media="all" rel="stylesheet" type="text/css" />

{{-- Data Table--}}
<link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="//cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css" rel="stylesheet">
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
@stack('css')


</head>
<body>

<div id="wrapper">


@include('include.header')


<div class="container-area">
     
     	<div class="container-fluid">
        <div class="row">
        
        	<div class="col-md-12 col-sm-12">
            
            	
                    <section class="content-header clearfix">
                      <h1>
                       @yield('page-title')
                      </h1>
                      <ol class="breadcrumb">
                        <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">@yield('page-title')</li>
                      </ol>
                    </section>     
                    
                    @include('include.flash-message')
					
                    @yield('content')
            
            </div><!-- end of col 12 -->
        </div><!-- end of row -->
		</div><!-- end of container -->
        

</div><!-- end of container-area -->

@include('include.footer')
    
<!--div id="back-to-top"><span><i class="fa fa-arrow-up"></i></span></div--> 
</div><!-- end of wrapper -->




<script src="{{ asset('frontend/js/jquery.min.js') }}" type="text/javascript"></script> 
<script src="{{ asset('frontend/js/modernizr.js') }}" type="text/javascript"></script>
    <!--[if IE]>
	<script src="js/html5.js"></script>
	<![endif]-->

<script src="{{ asset('frontend/js/bootstrap.min.js') }}" type="text/javascript"></script>

<link href="{{ asset('frontend/css/bootstrap-select.css') }}" media="all" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{ asset('frontend/js/bootstrap-select.js') }}"></script> 

<script type="text/javascript" src="{{ asset('frontend/js/custom.js') }}"></script> 

<!--jquery validate -->
<script src="{!! asset('frontend/js/jquery.validate.min.js')!!}"></script>
{{-- Data Table--}}
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/responsive/2.2.0/js/dataTables.responsive.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
@stack('js')
</body>
</html>
