{{-- <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Arvind University') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Arvind University') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html> --}}


<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7" lang="en-us"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8" lang="en-us"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie10 lt-ie9" lang="en-us"> <![endif]-->
<!--[if IE 9]> <html class="no-js lt-ie10 lt-ie9" lang="en-us"> <![endif]-->
<!--[if lt IE 10]> <html class="no-js lt-ie10" lang="en-us"> <![endif]-->
<!--[if !IE]> > <![endif]-->
<html class='no-js' lang='en'>
<!-- <![endif] -->
<head>
<meta name="description" content="" />
<meta name="author" content="" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<title>@yield('page-title') {{ config('app.name') }}</title>

<meta content='initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width' name='viewport' />
<meta content='yes' name='apple-mobile-web-app-capable'>
<meta content='translucent-black' name='apple-mobile-web-app-status-bar-style'>
<link href='{{ asset('frontend/images/favicon.png') }}' rel='shortcut icon'>
<link href='{{ asset('frontend/images/favicon.ico') }}' rel='icon' type='image/ico'>

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>

<link href="{{ asset('frontend/css/style.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('frontend/css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('frontend/css/font-awesome.min.css') }}" media="all" rel="stylesheet" type="text/css" />

<link href="{{ asset('frontend/css/streamline-small.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('frontend/css/streamline-large.css') }}" media="all" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/dev.css')}}">
@stack('css')
<script src="{{ asset('frontend/js/modernizr.js') }}" type="text/javascript"></script>
    <!--[if IE]>
	<script src="js/html5.js"></script>
	<![endif]-->

</head>
<body>

<div id="wrapper">

    <div class="login-regi-div clearfix">
                
        <div class="logo-box">
            <span class="logo-txt"><a href="{{ url('/') }}"><img src="{{ asset('frontend/images/logo.png') }}" class="img-responsive" alt=""></a></span>
        </div>

        @include('include.flash-message')

        @yield('content')

    </div><!-- end of login-regi-div -->

<p class="log-reg-footer-txt">&copy; 2018 Arvind University
   <!--  <span><a href="#">Contact</a><a href="#">Privacy Policy</a><a href="#">Terms of Use</a></span></p> -->

</div><!-- end of wrapper -->

<script src="{{ asset('frontend/js/jquery.min.js') }}" type="text/javascript"></script> 
<script src="{{ asset('frontend/js/bootstrap.min.js') }}" type="text/javascript"></script>
<link href="{{ asset('frontend/css/hover-dropdown-menu.css') }}" rel="stylesheet" />
<script type="text/javascript" src="{{ asset('frontend/js/hover-dropdown-menu.js') }}"></script>
<!-- Menu jQuery Bootstrap Addon -->
<script type="text/javascript" src="{{ asset('frontend/js/jquery.hover-dropdown-menu-addon.js') }}"></script> 

<script type="text/javascript" src="{{ asset('frontend/js/home-roundbox-hover.js') }}"></script> 

<script type="text/javascript" src="{{ asset('frontend/js/custom.js') }}"></script> 
<script src='https://www.google.com/recaptcha/api.js'></script>
@stack('js')
</body>
</html>

