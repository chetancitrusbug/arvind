
      <nav class="navbar navbar-expand-lg navbar-light bg-faded">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" data-toggle="collapse" class="navbar-toggle d-lg-none float-left"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            
            {{-- <form role="search" class="navbar-form navbar-right mt-1">
              <div class="position-relative has-icon-right">
                <input type="text" placeholder="Search" class="form-control round"/>
                <div class="form-control-position"><i class="ft-search"></i></div>
              </div>
            </form> --}}
          </div>
          <div class="navbar-container">
            <div id="navbarSupportedContent" class="collapse navbar-collapse">
              <ul class="navbar-nav">
                

                {{-- 
                  <li class="nav-item mr-2"><a id="navbar-fullscreen" href="javascript:;" class="nav-link apptogglefullscreen"><i class="ft-maximize font-medium-3 blue-grey darken-4"></i>
                    <p class="d-none">fullscreen</p></a></li>
                  
                  <li class="dropdown nav-item"><a id="dropdownBasic3" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle"><i class="ft-flag font-medium-3 blue-grey darken-4"></i><span class="selected-language d-none"></span></a>
                  <div class="dropdown-menu dropdown-menu-right"><a href="javascript:;" class="dropdown-item py-1"><img src="{{ asset('app-assets/img/flags/us.png') }}" class="langimg"/><span> English</span></a><a href="javascript:;" class="dropdown-item py-1"><img src="/app-assets/img/flags/es.png" class="langimg"/><span> Spanish</span></a><a href="javascript:;" class="dropdown-item py-1"><img src="/app-assets/img/flags/br.png" class="langimg"/><span> Portuguese</span></a><a href="javascript:;" class="dropdown-item"><img src="/app-assets/img/flags/de.png" class="langimg"/><span> French</span></a></div>
                </li> 
                --}}
                @if(!Auth::user()->can('access.home'))
                <li class="dropdown nav-item"><a id="dropdownBasic2" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle"><i class="ft-bell font-medium-3 blue-grey darken-4"></i><span class="notification badge badge-pill badge-danger">{{count($totalNotification)}}</span>
                    <p class="d-none">Notifications</p></a>
                  	<div class="notification-dropdown dropdown-menu dropdown-menu-right">
					@if(count($totalNotification) > 0)
					<div class="noti-list">
						@foreach($totalNotification as $key => $value)
						
					<a href="{{url('admin/batch/notification/').'/'.$value->notification_id}}" class="dropdown-item noti-container py-3"><i class="ft-bell danger float-left d-block font-large-1 mt-1 mr-2"></i><span class="noti-wrapper"><span class="noti-title line-height-1 d-block text-bold-400 danger">Batch Reminder</span><span class="noti-text">Batch#{{$value->batch_id.' schedule on '.$value->date}}</span></span></a>

						@endforeach
					</div>
					  
					  @else
					  <h3>You don't have any notification</h3>
					  @endif
                  </div>
                </li> 
                @endif
                <li class="dropdown nav-item">
                  <a id="dropdownBasic3" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle"><i class="ft-user font-medium-3 blue-grey darken-4"></i>
                    <p class="d-none">User Settings</p>
                  </a>
                  <div ngbdropdownmenu="" aria-labelledby="dropdownBasic3" class="dropdown-menu dropdown-menu-right">

					@if(!Auth::user()->can('access.home'))
                    <a href="{{ route('profile.edit') }}" class="dropdown-item py-1" title="Edit Profile">
                        <i class="fa fa-edit" aria-hidden="true"></i> Edit Profile
                    </a>
                    <a href="{{ route('profile.password') }}" class="dropdown-item py-1" title="Change Password">
                        <i class="fa fa-lock" aria-hidden="true"></i> Change Password
                    </a>
					@endif
                    <a href="#" class="dropdown-item" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" ><i class="ft-power mr-2"></i><span>Logout</span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                  </div>
                </li>

                {{-- <li class="nav-item"><a href="javascript:;" class="nav-link position-relative notification-sidebar-toggle"><i class="ft-align-left font-medium-3 blue-grey darken-4"></i>
                    <p class="d-none">Notifications Sidebar</p></a></li> --}}
              </ul>
            </div>
          </div>
        </div>
      </nav>
