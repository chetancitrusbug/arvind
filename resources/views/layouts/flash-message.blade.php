@if (Session::has('flash_warning'))

    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert"
                aria-hidden="true">&times;</button>
        {{ Session::get('flash_warning') }}
    </div>
@endif

@if (Session::has('flash_success'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert"
                aria-hidden="true">&times;</button>
        {{ Session::get('flash_success') }}
    </div>


@endif

@if (Session::has('flash_error'))
    <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert"
                aria-hidden="true">&times;</button>
        {{ Session::get('flash_error') }}
    </div>
@endif

@if (Session::has('flash_message'))

    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert"
                aria-hidden="true">&times;</button>
        {{ Session::get('flash_message') }}
    </div>
@endif
@if ($errors->any())
	<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert"
			aria-hidden="true">&times;</button>
		Please check the form below for errors
	</div>
@endif 