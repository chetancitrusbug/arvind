<!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
<div data-active-color="white" data-background-color="man-of-steel"  class="app-sidebar">
        <!-- main menu header-->
        <!-- Sidebar Header starts-->
        <div class="sidebar-header">
          <div class="logo clearfix">
              <div class="logo-img">
                  <a href="{{ url('/') }}" class="logo-text float-left">
                    <img src="{{ asset('frontend/images/logo.png') }}" width="180px"/>
                  </a>
                </div>
                <span class="text align-middle">  </span>
                
                <!--<a id="sidebarToggle" href="javascript:;" class="nav-toggle d-none d-sm-none d-md-none d-lg-block"><i data-toggle="expanded" class="ft-toggle-right toggle-icon"></i></a>
                  <a id="sidebarClose" href="javascript:;" class="nav-close d-block d-md-block d-lg-none d-xl-none"><i class="ft-x"></i></a>-->
                
                </div>
        </div>
        <!-- Sidebar Header Ends-->
        <!-- / main menu header-->
        <!-- main menu content-->
        <div class="sidebar-content">
          <div class="nav-container">
            <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">             
               
                <!--<li class="nav-item">

                  <li class="nav-item">
                    <a href="{{ url('admin/users') }}"><i class="ft-user"></i><span data-i18n="" class="menu-title">
                     Users</span>
                    </a>
				</li> 
                  <a href="{{ url('admin/roles') }}"><i class="ft-user"></i><span data-i18n="" class="menu-title">
                  Roles</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ url('admin/permissions') }}"><i class="ft-user"></i><span data-i18n="" class="menu-title">
                  Permissions</span>
                  </a>
				</li> -->
				@if(Auth::user()->can('access.employees'))
                <li class="nav-item">
                    <a href="{{ url('admin/employee') }}"><i class="fa fa-users"></i><span data-i18n="" class="menu-title">
                        Employees</span>
                    </a>
                </li>  
				@endif
				@if(Auth::user()->can('access.vendors'))
                <li class="nav-item">
                    <a href="{{ url('admin/vendor') }}"><i class="fa fa-users"></i><span data-i18n="" class="menu-title">
                      Vendors</span>
                    </a>
				</li>
				@endif
				@if(Auth::user()->can('access.trainers'))
                <li class="nav-item">
                    <a href="{{ url('admin/trainer') }}"><i class="fa fa-users"></i><span data-i18n="" class="menu-title">
                        Trainers</span>
                    </a>
				</li>
				@endif
				@if(Auth::user()->can('access.programs'))
                <li class="nav-item">
                    <a href="{{ url('admin/program') }}"><i class="fa fa-list"></i><span data-i18n="" class="menu-title">
                        Program</span>
                    </a>
				</li>
                @endif
                @if(Auth::user()->can('access.programs.select'))
                <li class="nav-item">
                    <a href="{{ url('admin/program-select') }}"><i class="fa fa-list"></i><span data-i18n="" class="menu-title">
                        Program</span>
                    </a>
				</li>
				@endif
				@if(Auth::user()->can('access.batch'))
                <li class="nav-item">
                    <a href="{{ url('admin/batch') }}"><i class="fa fa-list"></i><span data-i18n="" class="menu-title">
                        Batch</span>
                    </a>
				</li>
				@endif
				@if(Auth::user()->can('access.home'))
				<li class="nav-item">
					<a href="{{ url('batchuser/batch') }}"><i class="fa fa-list"></i><span data-i18n="" class="menu-title">
						Batch</span>
					</a>
				</li>
				@endif
            </ul>
          </div>
        </div>
        <!-- main menu content-->
        <div class="sidebar-background"></div>
        <!-- main menu footer-->
        <!-- include includes/menu-footer-->
        <!-- main menu footer-->
      </div>