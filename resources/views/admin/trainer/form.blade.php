
<div class="form-group{{ $errors->has('trainer_name') ? ' has-error' : ''}}">
    {!! Form::label('trainer_name', '* Name: ', ['class' => 'control-label']) !!} 
    {!! Form::text('trainer_name', null, ['class' => 'form-control col-md-4']) !!} {!! $errors->first('trainer_name', '<p class="help-block">:message</p>') !!}
</div>
@if(isset($trainer->email))
<div class="form-group{{ $errors->has('trainer_code') ? ' has-error' : ''}}">
    {!! Form::label('trainer_code', '* trainer Code: ', ['class' => 'control-label']) !!} 
    {!! Form::text('trainer_code', null, ['class' => 'form-control col-md-4','disabled' => 'disabled']) !!} {!! $errors->first('trainer_code', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', '* Email: ', ['class' => 'control-label']) !!} 
    {!! Form::text('email', null, ['class' => 'form-control col-md-4','disabled' => 'disabled']) !!} 
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
@else
<div class="form-group{{ $errors->has('trainer_code') ? ' has-error' : ''}}">
    {!! Form::label('trainer_code', '* trainer Code: ', ['class' => 'control-label']) !!} 
    {!! Form::text('trainer_code', null, ['class' => 'form-control col-md-4']) !!} {!! $errors->first('trainer_code', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', '* Email: ', ['class' => 'control-label']) !!} 
    {!! Form::text('email', null, ['class' => 'form-control col-md-4']) !!} 
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
@endif   
<div class="form-group{{ $errors->has('program') ? ' has-error' : ''}}">
    {!! Form::label('program', '* Program:', ['class' => 'control-label']) !!}
    </br>
    {!! Form::select('program[]', $program, null, ['id' => 'program','class' => 'form-control col-md-4' ,'multiple' => 'multiple']) !!}
    {!! $errors->first('program', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('vendor_id') ? ' has-error' : ''}}">
    {!! Form::label('vendor_id', '* Vendor:', ['class' => 'control-label']) !!}
    {!! Form::select('vendor_id', $vendor, isset($trainer->vendor_id) ? $trainer->vendor_id : [], ['id' => 'vendor_id','class' => 'form-control col-md-4']) !!}
    {!! $errors->first('vendor_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('address') ? ' has-error' : ''}}">
    {!! Form::label('address', '* Address: ', ['class' => 'control-label']) !!} 
    {!! Form::textarea('address', null, ['class' => 'form-control col-md-4']) !!} {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('mobile') ? ' has-error' : ''}}">
    {!! Form::label('mobile', '* Mobile No: ', ['class' => 'control-label']) !!} 
    {!! Form::text('mobile', null, ['class' => 'form-control col-md-4']) !!} {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', '* Status', ['class' => 'control-label']) !!}
    {!! Form::select('status',['1'=>'Active','0'=>'Inactive'] ,@$trainer->status, ['class' => 'form-control col-md-4']) !!} 
    {!! $errors->first('status','<p class="help-block with-errors">:message</p>') !!}
</div>
<div class="form-group">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
@push('js')
<script>
$(function() {
    $('#program').select2();
}); 
</script>
@endpush
