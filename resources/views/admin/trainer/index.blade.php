@extends('layouts.admin') 
@section('title',"Trainer") 
@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Trainer</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">

                    <a href="{{ url('/admin/trainer/create') }}" class="btn btn-success btn-sm"
                       title="Add New Trainer">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>
                
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
                <table class="table table-borderless" style="width:100%;" id="trainer-table">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile No</th>
                            <th>Vendor</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
        </div>
    </div>
</div>
@endsection
@push('js')

    <script>
        var url ="{{ url('/admin/trainer/datatable') }}";
        var edit_url = "{{ url('/admin/trainer') }}";
        var auth_check = "{{ Auth::check() }}";

    
        datatable = $('#trainer-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]], 
            ajax: {
                url:url,
                type:"get",
                data: function (d)  {
                    d.areas = $('#areas').val();
                }
            },
                columns: [
                    { data: 'trainer_code',name:'trainer_code',"searchable" : true}, 
                    { data: 'trainer_name',name:'trainer_name',"searchable" : true}, 
                    { data: 'email',name:'email',"searchable" : true}, 
                    { data: 'mobile',name:'mobile',"searchable" : true},
                    { data: 'vendor_name',name:'vendor_name',"searchable" : true}, 
                    { 
                        "data": null,
                        "name" : 'status',
                        "searchable": true,
                        "orderable": true,
                        "render": function (o) {
                            if(o.status == 0)
                            status = '<a href="'+edit_url+'/'+o.id+'?status=1" title="Active"><button class="btn btn-danger btn-xs"> @lang("forms.label.inactive")</button></a>';
                            else
                            status = "<a href='"+edit_url+"/"+o.id+"?status=0' data-id="+o.id+" title='Inactive'><button class='btn btn-success btn-xs'> @lang('forms.label.active')</button></a>";
                            return status;
                        }

                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e=""; var d= "";
                            
                            e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info btn-sm' title='Edit' ><i class='fa fa-edit' ></i></button></a>&nbsp;";  
                            d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" ><button class='btn btn-danger btn-sm' title='Delete' ><i class='fa fa-trash-o' aria-hidden='true'></i></button></a>&nbsp;"; 
                            
                            return e+d;
                        }

                    }
                ]
        });

        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            url = "{{url('admin/trainer')}}/" + id;
            var r = confirm("Are you sure you want to Delete Trainer?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success(data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            }
        });


    </Script>
    
@endpush