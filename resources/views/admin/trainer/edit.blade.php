@extends('layouts.admin') 
@section('title',"Edit Trainer") 
@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Edit Trainer  # {{$trainer->name}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="{{ url('/admin/trainer') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
                {{-- @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif --}}

            {!! Form::model($trainer, [
                'method' => 'PATCH',
                'url' => ['/admin/trainer', $trainer->id],
                'class' => 'form-horizontal',
                'id' => 'module_form',
                'autocomplete'=>'off'
            ]) !!}

            @include ('admin.trainer.form', ['submitButtonText' => 'Update'])

            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection