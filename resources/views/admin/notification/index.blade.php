@extends('layouts.admin') 
@section('title',"Notification") 
@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Notification</h4>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
                <table class="table table-borderless" style="width:100%;" id="notification-table">
                    <thead>
                        <tr>
                            <th>Batch Id</th>
                            <th>Batch Date</th>
                            <th>Meesage</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
        </div>
    </div>
</div>
@endsection
@push('js')

    <script>
        var url ="{{ url('/admin/notification/datatable') }}";
        var edit_url = "{{ url('/admin/notification') }}";
        var auth_check = "{{ Auth::check() }}";

    
        datatable = $('#notification-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]], 
            ajax: {
                url:url,
                type:"get",
                data: function (d)  {
                    d.areas = $('#areas').val();
                }
            },
                columns: [
                    { data: 'batch_id',name:'batch_id',"searchable" : true}, 
                    { data: 'date',name:'date',"searchable" : true}, 
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var c= "Batch#"+o.batch_id+' schedule on '+o.date;
                            return c;
                        }

                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var c=""; 
                            if(o.read_status == 0)
                            c = "<input type='checkbox' class='read_status' value='"+o.id+"' >";  
                            else
                            c = "<input type='checkbox' class='read_status' value='"+o.id+"' checked>";
                            return c;
                        }

                    }
                ]
        });

        $(document).on('click', '.read_status', function (e) {
            var id = $(this).attr('value');
            url = "{{url('admin/notification/read_status')}}";
            if (this.checked) {
                action = 1;
            }
            else{
                action = 0;
            }
                $.ajax({
                    type: "get",
                    url: url ,
                    data:{action:action,notification_id:id},
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success(data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            
        });

    </Script>
    
@endpush