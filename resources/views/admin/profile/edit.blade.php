@extends('layouts.admin')

@section('title','Edit Profile')

@section('content')
<div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">
                    <h4 class="card-title"> Edit Profile</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <!-- button & search bar -->
                    <a href="javascript:history.back()" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                               Back
                            </button>
    
                        </a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card-block">
            
                 <!-- Put Table structure & Forms here -->
                {{-- @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif --}}
    
                        {!! Form::model($user,[
                            'method' => 'PATCH',
                            'class' => 'form-horizontal',
                            'files'=>true,
                            'autocomplete'=>'off'
                        ]) !!}
    
                       
    
    
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
                            <label for="name" class="col-md-4 control-label">
                                <span class="field_compulsory">*</span>Name
                            </label>
                            <div class="col-md-6">
                                {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        
                        
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
                            {!! Form::label('email','Email', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {{--<p style="border: 1px solid #d0d0d0; padding: 5px">{{$user->email}}</p>--}}
                                {!! Form::email('email', isset($user->email)?$user->email:old('email'), ['class' => 'form-control', 'required' => 'required','disabled'=>true]) !!}
                                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
    
                       
    
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update Profile', ['class' => 'btn btn-primary']) !!}
                               
                            </div>
                        </div>
    
                        {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection