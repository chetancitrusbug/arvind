@extends('layouts.admin')

@section('title',"Permissions")

@section('content')
<div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">
                    <h4 class="card-title"> Permissions </h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <!-- button & search bar -->
                    @if(Auth::user()->can('access.permission.create'))
                    <a href="{{ url('/admin/permissions/create') }}" class="btn btn-success btn-sm"
                       title="Add New Permission">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>
                @endif
    
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/permissions', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            {!! Form::close() !!}
                    
                            
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card-block">
            
                 <!-- Put Table structure & Forms here -->
                 <table class="table table-bordered">

                        @foreach($permissions as $item)

                            <th colspan="3">{{$item->label}} [ <i class="text-success">{{$item->name}}</i> ]
                                <div class="roles" style="float: right;">
                                    @if($item->roles)
                                        @foreach($item->roles as $k=>$role)
                                            @if($role->name!="SU") <a href="{{url('admin/roles')}}/{{$role->id}}" title="{{$role->label}}">{{$role->name}}</a> @if($item->roles->count() !=$k+1)| @endif @endif
                                        @endforeach
                                    @endif
                                </div>
                            </th>

                            <tr>
                                <td>
                                    <a href="{{ url('/admin/permissions/' . $item->id) }}" title="View Permission"
                                       class=" btn btn-info btn-xs">
                                        <i class="fa fa-eye"
                                           aria-hidden="true"></i>

                                    </a>

                                    @if(Auth::user()->can('access.permission.edit'))
                                        <a href="{{ url('/admin/permissions/' . $item->id . '/edit') }}"
                                           class="btn btn-primary btn-xs"
                                           title="Edit Permission">
                                            <i class="fa fa-pencil-square-o"
                                               aria-hidden="true"></i>

                                        </a>
                                    @endif


                                    <p>
                                        {{$item->description or ''}}
                                    </p>
                                </td>

                                <td>

                                    <table class="table table-bordered">
                                        <th colspan="3">Child Permissions</th>

                                        @if($item->child)
                                            @foreach($item->child as $per)
                                                <tr>

                                                    <td class="col-md-5">
                                                        {{$per->label}} [ <i class="text-success">{{$per->name}}</i> ]
                                                    </td>

                                                    <td>

                                                        <a href="{{ url('/admin/permissions/' . $per->id) }}"
                                                           class="btn btn-info btn-xs"
                                                           title="View Permission">
                                                            <i class="fa fa-eye"
                                                               aria-hidden="true"></i>
                                                            

                                                        </a>
                                                        @if(Auth::user()->can('access.permission.edit'))

                                                            <a href="{{ url('/admin/permissions/' . $per->id . '/edit') }}"
                                                               class="btn btn-primary btn-xs"
                                                               title="Edit Permission">
                                                                <i
                                                                        class="fa fa-pencil-square-o"
                                                                        aria-hidden="true"></i> 
                                                            </a>
                                                        @endif

                                                        @if(Auth::user()->can('access.permission.delete'))
                                                            {!! Form::open([
                                                                                                      'method' => 'DELETE',
                                                                                                      'url' => ['/admin/permissions', $per->id],
                                                                                                      'style' => 'display:inline'
                                                                                                  ]) !!}
                                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete ', array(
                                                                    'type' => 'submit',
                                                                    'class' => 'btn btn-danger btn-xs',
                                                                    'title' => 'Delete Permission',
                                                                    'onclick'=>"return confirm('Confirm Delete?')"
                                                            )) !!}
                                                            {!! Form::close() !!}
                                                        @endif

                                                    </td>
                                                </tr>

                                            @endforeach
                                        @endif
                                    </table>
                                </td>

                            </tr>

                        @endforeach
                    </table>
                {{-- <table class="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th>ID</th><th>Name</th><th>Label</th><th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($permissions as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td><a href="{{ url('/admin/permissions', $item->id) }}">{{ $item->name }}</a></td><td>{{ $item->label }}</td>
                                            <td>
                                                <a href="{{ url('/admin/permissions/' . $item->id) }}" title="View Permission"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                                <a href="{{ url('/admin/permissions/' . $item->id . '/edit') }}" title="Edit Permission"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                                {!! Form::open([
                                                    'method' => 'DELETE',
                                                    'url' => ['/admin/permissions', $item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-danger btn-xs',
                                                            'title' => 'Delete Permission',
                                                            'onclick'=>'return confirm("Confirm delete?")'
                                                    )) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="pagination"> {!! $permissions->appends(['search' => Request::get('search')])->render() !!} </div> --}}
            </div>
        </div>
    </div>
@endsection
