@extends('layouts.admin') 
@section('title',"Permissions") 
@section('content')
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title"> Permission </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <!-- button & search bar -->

                <a href="{{ url('/admin/permissions') }}" title="Back">
                    <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                    </button>
                </a>


                @if(Auth::user()->can('access.permission.edit'))
                    <a href="{{ url('/admin/permissions/' . $permission->id . '/edit') }}" title="Edit Permission">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                  aria-hidden="true"></i>
                           Edit
                        </button>
                    </a>
                @endif

                @if(Auth::user()->can('access.permission.delete'))

                    {!! Form::open([
                        'method' => 'DELETE',
                        'url' => ['/admin/permissions', $permission->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete Permission',
                            'onclick'=>"return confirm('Cofirm Delete?')"
                    ))!!}
                    {!! Form::close() !!}
                @endif

            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">

            <!-- Put Table structure & Forms here -->
            <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Label</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{ $permission->id }}</td>
                        <td> {{ $permission->name }} </td>
                        <td> {{ $permission->label }} </td>
                    </tr>
                    </tbody>
                </table>
        </div>
    </div>
</div>
@endsection