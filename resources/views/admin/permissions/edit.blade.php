@extends('layouts.admin')

@section('title',"Permissions")


@section('content')
<div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">
                    <h4 class="card-title"> Permissions </h4>
                    <div class="actions">
                            @include('partials.page_tooltip',['model' => 'permission','page'=>'form'])
                        </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <!-- button & search bar -->
                     <a href="{{ url('/admin/permissions') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    
                            
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card-block">
            
                 <!-- Put Table structure & Forms here -->
                 @if ($errors->any())
                 <ul class="alert alert-danger">
                     @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
                     @endforeach
                 </ul>
             @endif

             {!! Form::model($permission, [
                 'method' => 'PATCH',
                 'url' => ['/admin/permissions', $permission->id],
                 'class' => 'form-horizontal',
                 'autocomplete'=>'off'
             ]) !!}

             @include ('admin.permissions.form', ['submitButtonText' => 'Update'])

             {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection