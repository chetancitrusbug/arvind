@extends('layouts.admin')

@section('title',"Permissions")

@section('content')
<div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">
                    <h4 class="card-title"> Create Permission </h4>
                    <div class="actions">
                            @include('partials.page_tooltip',['model' => 'permission','page'=>'form'])
                        </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <!-- button & search bar -->
                    <a href="{{ url('/admin/permissions') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card-block">
            
                 <!-- Put Table structure & Forms here -->
                 @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/permissions', 'class' => 'form-horizontal','autocomplete'=>'off']) !!}

                        @include ('admin.permissions.form')

                        {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection