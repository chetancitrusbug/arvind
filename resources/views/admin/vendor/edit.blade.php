@extends('layouts.admin') 
@section('title',"Edit Vendor") 
@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Edit Vendor  # {{$vendor->name}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="{{ url('/admin/vendor') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
                {{-- @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif --}}

            {!! Form::model($vendor, [
                'method' => 'PATCH',
                'url' => ['/admin/vendor', $vendor->id],
                'class' => 'form-horizontal',
                'id' => 'module_form',
                'autocomplete'=>'off'
            ]) !!}

            @include ('admin.vendor.form', ['submitButtonText' => 'Update'])

            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection