@extends('layouts.admin') 
@section('title',"Vendor") 
@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Vendor Trainer List</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="{{ url('/admin/vendor') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
                <table class="table table-borderless" style="width:100%;" id="trainer-table">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile No</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                </table>
        </div>
    </div>
</div>
@endsection
@push('js')

    <script>
    var id = "{{$id}}";
        var url ="{{ url('/admin/vendor/listTrainerDatatable') }}"+'/'+id;
        var auth_check = "{{ Auth::check() }}";

    
        datatable = $('#trainer-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]], 
            ajax: {
                url:url,
                type:"get",
            },
                columns: [
                    { data: 'trainer_code',name:'trainer_code',"searchable" : true}, 
                    { data: 'trainer_name',name:'trainer_name',"searchable" : true}, 
                    { data: 'email',name:'email',"searchable" : true}, 
                    { data: 'mobile',name:'mobile',"searchable" : true},
                    { 
                        "data": null,
                        "name" : 'status',
                        "searchable": true,
                        "orderable": true,
                        "render": function (o) {
                            if(o.status == 0)
                            status = 'Inactive';
                            else
                            status = "Active";
                            return status;
                        }

                    }
                ]
        });

    </Script>
    
@endpush