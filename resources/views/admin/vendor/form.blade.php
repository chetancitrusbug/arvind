
<div class="form-group{{ $errors->has('vendor_name') ? ' has-error' : ''}}">
    {!! Form::label('vendor_name', '* Name: ', ['class' => 'control-label']) !!} 
    {!! Form::text('vendor_name', null, ['class' => 'form-control col-md-4']) !!} {!! $errors->first('vendor_name', '<p class="help-block">:message</p>') !!}
</div>
@if(isset($vendor->email))
<div class="form-group{{ $errors->has('vendor_code') ? ' has-error' : ''}}">
    {!! Form::label('vendor_code', '* Vendor Code: ', ['class' => 'control-label']) !!} 
    {!! Form::text('vendor_code', null, ['class' => 'form-control col-md-4','disabled' => 'disabled']) !!} {!! $errors->first('vendor_code', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', '* Email: ', ['class' => 'control-label']) !!} 
    {!! Form::text('email', null, ['class' => 'form-control col-md-4','disabled' => 'disabled']) !!} 
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
@else
<div class="form-group{{ $errors->has('vendor_code') ? ' has-error' : ''}}">
    {!! Form::label('vendor_code', '* Vendor Code: ', ['class' => 'control-label']) !!} 
    {!! Form::text('vendor_code', null, ['class' => 'form-control col-md-4']) !!} {!! $errors->first('vendor_code', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', '* Email: ', ['class' => 'control-label']) !!} 
    {!! Form::text('email', null, ['class' => 'form-control col-md-4']) !!} 
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
@endif   
<div class="form-group{{ $errors->has('address1') ? ' has-error' : ''}}">
    {!! Form::label('address1', '* Address 1: ', ['class' => 'control-label']) !!} 
    {!! Form::textarea('address1', null, ['class' => 'form-control col-md-4']) !!} {!! $errors->first('address1', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('address2') ? ' has-error' : ''}}">
    {!! Form::label('address2', '* Address 2: ', ['class' => 'control-label']) !!} 
    {!! Form::textarea('address2', null, ['class' => 'form-control col-md-4']) !!} {!! $errors->first('address2', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('mobile') ? ' has-error' : ''}}">
    {!! Form::label('mobile', '* Mobile No: ', ['class' => 'control-label']) !!} 
    {!! Form::text('mobile', null, ['class' => 'form-control col-md-4']) !!} {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', '* Status', ['class' => 'control-label']) !!}
    {!! Form::select('status',['1'=>'Active','0'=>'Inactive'] ,@$vendor->status, ['class' => 'form-control col-md-4']) !!} 
    {!! $errors->first('status','<p class="help-block with-errors">:message</p>') !!}
</div>
<div class="form-group">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
