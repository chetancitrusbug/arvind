@push('css')
{{--<style>
    .checkbox label {
        position: inherit !important;
    }
    .checkbox input[type="checkbox"] {
        opacity: 1 !important;
    }
</style>--}}
@endpush

<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', '* Name: ', ['class' => 'control-label']) !!} 
    {!! Form::text('name', null, ['class' => 'form-control col-md-4', 'required' => 'required']) !!} {!! $errors->first('name', '
    <p class="help-block">:message</p>') !!}
</div>
@if(isset($user->email))
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', '* Email: ', ['class' => 'control-label']) !!} 
    {!! Form::text('email', null, ['class' => 'form-control col-md-4','disabled' => 'disabled']) !!} 
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
@else
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', '* Email: ', ['class' => 'control-label']) !!} 
    {!! Form::text('email', null, ['class' => 'form-control col-md-4', 'required' => 'required']) !!} 
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
    {!! Form::label('password', '* Password: ', ['class' => 'control-label']) !!}
    {!! Form::password('password',  ['class' => 'form-control col-md-4', 'required' => 'required']) !!}
    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
</div>
@endif   
<div class="form-group{{ $errors->has('phone') ? ' has-error' : ''}}">
    {!! Form::label('phone', '* Phone No: ', ['class' => 'control-label']) !!} 
    {!! Form::text('phone', null, ['class' => 'form-control col-md-4', 'required' => 'required']) !!} {!! $errors->first('phone', '
    <p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', '* Status', ['class' => 'control-label']) !!}
    {!! Form::select('status',['1'=>'Active','0'=>'Inactive'] ,@$user->status, ['class' => 'form-control col-md-4', 'required' => 'required']) !!} 
    {!! $errors->first('status','<p class="help-block with-errors">:message</p>') !!}
</div>
<div class="form-group">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
@section('footerExtra')
@push('js')

    <script>
       $('.parent').change(function (e) {

            var $this = $(this),
                parent = $this.data('parent'),
                child = $('.child-' + parent);

            if ($this.is(':checked')) {
                child.prop('checked', true);
            } else {
                child.prop('checked', false);
            }
            e.preventDefault();
        });
        

    </script>
@endpush
@endsection