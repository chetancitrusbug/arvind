@extends('layouts.admin') 
@section('title',"Users") 
@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Users</h4>
            </div>
        </div>
        <div class="row">
           <!-- <div class="col-12">

                    <a href="{{ url('/admin/users/create') }}" class="btn btn-success btn-sm"
                       title="Add New User">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>
                
            </div> -->
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
                <table class="table table-borderless" style="width:100%;" id="users-table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>status</th>
                        </tr>
                    </thead>
                </table>
        </div>
    </div>
</div>
@endsection
@push('js')

    <script>
        var url ="{{ url('/admin/users-data') }}";
        var edit_url = "{{ url('/admin/users') }}";
        var auth_check = "{{ Auth::check() }}";

    
        datatable = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]], 
            ajax: {
                url:url,
                type:"get",
            },
                columns: [
                    { data: 'created_at',name : 'created_at',"visible":false},
                    { data: 'id',name : 'id',"searchable": true, "orderable": true},
                    { data: 'name',name : 'name',"searchable": true, "orderable": true},
                    { data: 'email',name : 'email',"searchable": true, "orderable": true},
                    { 
                        "data": null,
                        "name" : 'status',
                        "searchable": true,
                        "orderable": true,
                        "render": function (o) {
                            if(o.status == 1)
                                return 'Active';
                            else
                                return 'Inactive';
                        }

                    }
                ]
        });

        $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        url = "{{url('admin/users')}}/" + id;
        var r = confirm("Are you sure you want to Delete User?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success(data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procced!',erro)
                }
            });
        }
    });


        $('#areas').change(function() {
            datatable.draw();
        });

    </Script>
    
@endpush