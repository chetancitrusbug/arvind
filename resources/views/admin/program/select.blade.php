@extends('layouts.admin') 
@section('title',"Select Programs") 
@section('content')

<div class="row">
    <div class="col-lg-12">
	<?php /*
        @if($select_status == 1 )
            <h3>Thankyou, for your program selection, your request has been submitted !!</h3>
        @elseif($select_status == 2 )
            <h3>Your request is cancelled by the manager. Please select another programs and re-submit your request. </h3>
        @elseif($select_status == 3 )
            {{-- <h3>Your selected programs have been approved by the manager.</h3> --}}
            <h3>Thankyou, for your program selection, your programs have ben updated successfully !!</h3>
        @else
        @endif
	*/ ?>
    </div>
</div>

<div class="row">
    @foreach($category as $cat)
    <div class="col-lg-3">
        <div class="card cat-list">
            <div class="card-header bg-maroon bg-purple">
                <h4 class="card-title" id="cat_{{ $cat->id }}">{{ $cat->cat_name }}</h4>
            </div>
            <div class="card-body">
                <div class="card-block ">
                    <div class="card-category-list">
                        @foreach($cat->program as $program)
                        <div class="check-list">
                            <div class="checkboxes">
                                <input type="checkbox"   @if(in_array($program->id,$selected_programs))  checked @endif  @if($select_status == 1 || $select_status == 3) disabled @endif  id="build" class="program-select" data-category-name="{{ $cat->cat_name }}" data-category-id="{{ $cat->id }}" data-program-id="{{ $program->id }}"  data-program-name="{{ $program->program_name }}" value="{{ $program->id }}" >
                                <label class="toggle" for="build"></label>
                            </div>
                            <div class="option-name">
                                <p> {{ $program->program_name }} </p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
<form id="program-select-form" method="post" action="{{ route('program.select.submit') }}">
    @csrf
<div class="category-selected">
    <div class="row">
        <div class="col-lg-12">
            <h3>Category</h3>
                <div id="progdiv"></div>
                {{-- <span class="tags bg-purple">Option one</span>
                <span class="tags">Option Tow</span>
                <span class="tags bg-red">Option Three</span>
                <span class="tags bg-green">Option Four</span>
                <span class="tags bg-skyblue">Option Five</span>
                <span class="tags">Option Six</span> --}}
        </div>
    </div>
</div>

@if($select_status != 1  && $select_status != 3)
<div class="row">
    <div class="col-lg-12 text-right">
        <div class="form-group">
            <input class="btn btn-primary btn-submit" type="submit" value="Submit">
            <input class="btn btn-primary btn-cancel" type="reset" value="Cancel">
        </div>
    </div>
</div>
@endif

</form>
</div>

@endsection

@push('js')
<script type="text/javascript">

    $(document).ready(function() {
            var select_status = parseInt("{{ $select_status }}");
            var program = []; 
            var total=$('input.program-select:checked').length;
            var colorclass = ["bg-purple", "bg-red", "bg-green", "bg-skyblue", ""];
            curr = 0;
            if(select_status == 1 || select_status == 3 ){
                $.each($("input.program-select:checked"), function(){ 
                    curr++;
                    if (curr == colorclass.length) {
                        curr = 0;
                    }
                    $('#progdiv').append('<span class="tags '+colorclass[curr]+'">'+$(this).data('program-name')+'</span>');
                });     
            }
            if(select_status == 2 ){
                $.each($("input.program-select:checked"), function(){ 
                    curr++;
                    if (curr == colorclass.length) {
                        curr = 0;
                    }
                    $('#progdiv').append('<input type="hidden"  name="id[]" value="'+$(this).data('program-id')+'" class="inputhidden'+$(this).val()+' inputcategory'+$(this).data('category-id')+' " />');
                    $('#progdiv').append('<span class="tags '+colorclass[curr]+' inputhidden'+$(this).val()+'">'+$(this).data('program-name')+'</span>');
                });     
            }

    });

    
    $(document).on('change', 'input.program-select', function(){

        var total = 0;
        var category_id = $(this).data('category-id') ;
        var category_name = $(this).data('category-name') ;
        var program_id = $(this).data('program-id') ;
        var program_name = $(this).data('program-name') ;
        if($(this).is(':checked')) {
            var query = (category_id != 4) ? 'categories_all' : ''  ;
            $('#progdiv').append('<input type="hidden"  name="id[]" value="'+program_id+'" class="inputhidden'+$(this).val()+' inputcategory'+category_id+' '+query+' " />');
            var total=$('input[name="id[]"]').length;
            var colorclass = "";
            if(total % 2 == 0){
                colorclass = 'bg-purple' ;
            }
            if(total % 3 == 0){
                colorclass = 'bg-red' ;
            }
            if(total % 4 == 0){
                colorclass = 'bg-green' ;
            }
            if(total % 5 == 0){
                colorclass = 'bg-skyblue' ;
            }
            $('#progdiv').append('<span class="tags '+colorclass+' inputhidden'+$(this).val()+'">'+program_name+'</span>');
            var total_cat = $('#progdiv').find('.inputcategory'+category_id).length;

            // get count of cat 1,2,3
            var total_cat_3 = 0 ;
            if(category_id == 1 || category_id == 2 || category_id == 3){
                total_cat_3 = $('#progdiv').find('.categories_all' ).length ;
            }

            if(category_id == 1 || category_id == 2 || category_id == 3 ){
                if( total_cat_3 > 3){ 
                    alert('you cannot select more than 3 programs');
                    $(this).prop('checked', false);
                    $('#progdiv').find('.inputhidden'+$(this).val()).remove();
                    return false;
                }
            }
            if(category_id == 4){
                if( total_cat > 4){ 
                    alert('you cannot select more than 4 programs from '+category_name);
                    $(this).prop('checked', false);
                    $('#progdiv').find('.inputhidden'+$(this).val()).remove();
                    return false;
                }
            }
            
        } else {
            $('#progdiv').find('.inputhidden'+$(this).val()).remove();
        }

});

$('#program-select-form').on('submit',  function(){
    if( $('input[name="id[]"]').length <= 0){
        alert('Please select atleast one Program');
        return false;
    }
    var cat_1 = 0 ; var cat_2 = 0 ; var cat_3 = 0 ;
    cat_1 = $('#progdiv').find('.inputcategory'+1).length;
    cat_2 = $('#progdiv').find('.inputcategory'+2).length;
    cat_3 = $('#progdiv').find('.inputcategory'+3).length;

    var total_from_3 = cat_1 + cat_2 +  cat_3 ;
    if(total_from_3 == 0){
        alert('Please select atleast one Program from '+$('#cat_1').html()+','+$('#cat_2').html()+','+$('#cat_3').html() +' categories');
        return false;
    }
  

    return true;
});

</script>
@endpush