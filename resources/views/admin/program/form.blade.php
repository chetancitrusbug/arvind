
<div class="form-group{{ $errors->has('program_name') ? ' has-error' : ''}}">
    {!! Form::label('program_name', '* Name: ', ['class' => 'control-label']) !!} 
    {!! Form::text('program_name', null, ['class' => 'form-control col-md-4']) !!} {!! $errors->first('program_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('classification_id') ? ' has-error' : ''}}">
    {!! Form::label('classification_id', '* Classification:', ['class' => 'control-label']) !!}
    {!! Form::select('classification_id', $classification, isset($program->classification_id) ? $program->classification_id : [], ['id' => 'classification_id','class' => 'form-control col-md-4']) !!}
    {!! $errors->first('classification_id', '<p class="help-block">:message</p>') !!}
</div>
@if(isset($category))
<div class="form-group{{ $errors->has('category_id') ? ' has-error' : ''}}">
    {!! Form::label('category_id', '* Category:', ['class' => 'control-label']) !!}
    {!! Form::select('category_id', $category, isset($program->category_id) ? $program->category_id : [], ['id' => 'category_id','class' => 'form-control col-md-4']) !!}
    {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
</div>  
@else
<div class="form-group{{ $errors->has('category_id') ? ' has-error' : ''}}">
        {!! Form::label('category_id', '* Category:', ['class' => 'control-label']) !!}
        {!! Form::select('category_id', ['' => 'Select Category'], isset($program->category_id) ? $program->category_id : [], ['id' => 'category_id','class' => 'form-control col-md-4']) !!}
        {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
    </div> 
@endif 
<div class="form-group{{ $errors->has('description') ? ' has-error' : ''}}">
    {!! Form::label('description', '* Description: ', ['class' => 'control-label']) !!} 
    {!! Form::textarea('description', null, ['class' => 'form-control col-md-4']) !!} {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('duration') ? ' has-error' : ''}}">
    {!! Form::label('duration', '* Duration Days: ', ['class' => 'control-label']) !!} 
    {!! Form::number('duration', null, ['class' => 'form-control col-md-4']) !!} {!! $errors->first('duration', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('attachement') ? ' has-error' : ''}}">
    {!! Form::label('attachement', '* Attachement: ', ['class' => 'control-label']) !!} 
    {!! Form::file('attachement', null, ['class' => 'form-control col-md-4']) !!} 
    {!! $errors->first('attachement', '<p class="help-block">:message</p>') !!}
</div>
@if(isset($program->attachement))
<div class="form-group">
    <a href={{ url($program->attachement) }} class ='btn btn-primary' download>Download</a>
</div>
@else
@endif 
<div class="form-group {{ $errors->has('level') ? 'has-error' : ''}}">
    {!! Form::label('level', '* Level', ['class' => 'control-label']) !!}
    {!! Form::select('level',[''=>'Select Level','1'=>'L1','2'=>'L2','3'=>'L3'] ,@$program->level, ['class' => 'form-control col-md-4']) !!} 
    {!! $errors->first('level','<p class="help-block with-errors">:message</p>') !!}
</div>
<div class="form-group">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
@push('js')
<script>
$(function() {
    $(document).on('change', '#classification_id', function (e) {
        var id = $('#classification_id').val();
        if(id != '')
        {
            var url ="{{ url('admin/getCategory/') }}";
            url = url + "/" + id;
            $.ajax({
                type: "get",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                        data = JSON.parse( data );
                        var select = $('#category_id');
                        select.empty();
                        select.append('<option value="">Select Category</option>');
                        $.each(data,function(key, value)
                        {
                            select.append('<option value=' + key + '>' + value + '</option>');
                        });
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
        else 
        {
            var select = $('#category_id');
            select.empty();
            select.append('<option value="">Select Category</option>');
        }
        
    });
}); 
</script>
@endpush