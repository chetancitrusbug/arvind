@extends('layouts.admin') 
@section('title',"Create program") 
@section('content')


<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Create New program</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <!-- button & search bar -->
                <a href="{{ url('/admin/program') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <!-- Put Table structure & Forms here -->
            {{-- @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif --}}
            {!! Form::open(['url' => '/admin/program', 'class' => 'form-horizontal','id' => 'module_form','files'=>true]) !!}

            @include ('admin.program.form')

            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection