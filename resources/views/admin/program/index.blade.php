@extends('layouts.admin') 
@section('title',trans('forms.label.program').' ') 
@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">@lang('forms.label.program')</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">

                    <a href="{{ url('/admin/program/create') }}" class="btn btn-success btn-sm"
                       title="@lang('forms.label.add_new') @lang('forms.label.program')">
                        <i class="fa fa-plus" aria-hidden="true"></i> @lang('forms.label.add_new')
                    </a>
                    <?php /*@if(Auth::user()->hasRole('SU'))
                    <a href="{{ route('program.upload') }}" class="btn btn-success btn-sm"
                       title="@lang('forms.label.uploadexcel') @lang('forms.label.program')">
                        <i class="fa fa-plus" aria-hidden="true"></i> @lang('forms.label.uploadexcellabel')
                    </a>
                    @endif
					*/ ?>
                
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <table class="table table-borderless" style="width:100%;" id="program-table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Classification</th>
                        <th>Level</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@push('js')

    <script>
        var url ="{{ url('/admin/program/datatable') }}";
        var edit_url = "{{ url('/admin/program') }}";
        var auth_check = "{{ Auth::check() }}";

    
        datatable = $('#program-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]], 
            ajax: {
                url:url,
                type:"get",
                data: function (d)  {
                    d.areas = $('#areas').val();
                }
            },
                columns: [
                    { data: 'program_name',name:'program_name',"searchable" : true},
                    { data: 'cat_name',name:'cat_name',"searchable" : true}, 
                    { data: 'classification_name',name:'classification_name',"searchable" : true},
                    { 
                        "data": null,
                        "name" : 'level',
                        "searchable": true,
                        "orderable": true,
                        "render": function (o) {
                            
                            return 'L'+o.level;
                        }

                    },
                    { 
                        "data": null,
                        "name" : 'status',
                        "searchable": true,
                        "orderable": true,
                        "render": function (o) {
                            
                            if(o.status == 0)
                            status = '<a href="'+edit_url+'/'+o.id+'?status=1" title="Active"><button class="btn btn-danger btn-xs"> @lang("forms.label.inactive")</button></a>';
                            else
                            status = "<a href='"+edit_url+"/"+o.id+"?status=0' data-id="+o.id+" title='Inactive'><button class='btn btn-success btn-xs'> @lang('forms.label.active')</button></a>";
                            return status;
                        }

                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e=""; var d= "";
                            e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info btn-sm' title='Edit' ><i class='fa fa-edit' ></i></button></a>&nbsp;";  
                            d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" ><button class='btn btn-danger btn-sm' title='Delete' ><i class='fa fa-trash-o' aria-hidden='true'></i></button></a>&nbsp;"; 
                            
                            return e+d;
                        }

                    }
                ]
        });

        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            url = "{{url('admin/program')}}/" + id;
            var r = confirm("Are you sure you want to Delete Program?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success(data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            }
        });


    </Script>
    
@endpush