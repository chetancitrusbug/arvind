@extends('layouts.admin') 
@section('title',"Create batch") 
@section('content')


<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Create New batch</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <!-- button & search bar -->
                <a href="{{ url('/admin/batch') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <!-- Put Table structure & Forms here -->
            <div class="errBlog">
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            </div>
            {!! Form::open(['url' => '/admin/batch', 'class' => 'form-horizontal','id' => 'batch_create_form']) !!}

            @include('admin.batch.form')
            

            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
