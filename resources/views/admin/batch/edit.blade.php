@extends('layouts.admin') 
@section('title',"Edit batch") 
@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Edit batch  # {{$batch->id}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="{{ url('/admin/batch') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <div class="errBlog">
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            </div>

            {!! Form::model($batch, [
                'method' => 'PATCH',
                'url' => ['/admin/batch', $batch->id],
                'class' => 'form-horizontal',
                'id' => 'module_form'
            ]) !!}
            
            @include ('admin.batch.form', ['submitButtonText' => 'Update'])

            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
@push('js')

    <script>
    $( document ).ready(function() {
        $('.emp_srch').show();
        idsArr = "{{ $batchEmpIds }}";
        idsArrString = "{{ $batchEmpIdsString }}";
        sessionStorage.setItem("checkedIds", idsArrString);
        if(idsArr != null )
        {
            $('#count').html("{{ $batchEmpIdsCount }}");
            $('.idchecklist').each(function () {
                $(this).prop("checked", ($.inArray($(this).val(), idsArr) !== -1) );
            }); 
        }
        
    });
    </script>
    @endpush
    