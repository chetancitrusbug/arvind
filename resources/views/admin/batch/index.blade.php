@extends('layouts.admin') 
@section('title',"batch") 
@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Batch</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">

                    <a href="{{ url('/admin/batch/create') }}" class="btn btn-success btn-sm"
                       title="Add New batch">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>
                
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <table class="table table-borderless" style="width:100%;" id="batch-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Date</th>
                        <th>Vendor</th>
                        <th>Trainner</th>
                        <th>Program</th>
                        <th>Approval</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@push('js')

    <script>
        var url ="{{ url('/admin/batch/datatable') }}";
        var edit_url = "{{ url('/admin/batch') }}";
        var auth_check = "{{ Auth::check() }}";

    
        datatable = $('#batch-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": false, 
            ajax: {
                url:url,
                type:"get",
                data: function (d)  {
                    d.areas = $('#areas').val();
                }
            },
                columns: [
                    { data: 'id',name:'id',"searchable" : true}, 
                    { data: 'start_date',name:'start_date',"searchable" : true}, 
                    { data: 'vendor_name',name:'vendor_name',"searchable" : true},
                    { data: 'trainer_name',name:'trainer_name',"searchable" : true},
                    { data: 'program_name',name:'program_name',"searchable" : true}, 
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var approval = '';
                            if(o.approval == 0)
                            approval = 'No';
                            else if(o.approval == 2)
                            approval = "Cancelled";
                            else
                            approval = "Yes";
                            return approval;
                        }
                    },
                    { 
                        "data": null,
                        "name" : 'status',
                        "searchable": true,
                        "orderable": true,
                        "render": function (o) {
                            
                            if(o.status == 0)
                            status = '<a href="'+edit_url+'/'+o.id+'?status=1" title="Active"><button class="btn btn-danger btn-xs"> @lang("forms.label.inactive")</button></a>';
                            else
                            status = "<a href='"+edit_url+"/"+o.id+"?status=0' data-id="+o.id+" title='Inactive'><button class='btn btn-success btn-xs'> @lang('forms.label.active')</button></a>";
                            return status;
                        }

                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e=""; var d= "";
                            e = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info btn-sm' title='View' ><i class='fa fa-eye' ></i></button></a>&nbsp;";  
                            d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" ><button class='btn btn-danger btn-sm' title='Delete' ><i class='fa fa-trash-o' aria-hidden='true'></i></button></a>&nbsp;"; 
                            
                            return e+d;
                        }

                    }
                ]
        });

        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            url = "{{url('admin/batch')}}/" + id;
            var r = confirm("Are you sure you want to Delete batch?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success(data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            }
        });


    </Script>
    
@endpush