@extends('layouts.admin') 
@section('title',"View batch") 
@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title"> batch # {{ $batch->id }} </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                    <a href="{{ url('/admin/batch') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
                <table class="table table-borderless">
                    <tbody>

                    <tr>
                        <td>Id</td>
                        <td>#{{ $batch->id }}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{{ $batch->start_date }}</td>
                    </tr>
                    <tr>
                        <td>Program Name</td>
                        <td> {{ ((isset($batch->program->program_name) ? $batch->program->program_name : '-')) }} </td>
                    </tr>
                    <tr>
                        <td>Vendor Name</td>
                        <td> {{ ((isset($batch->vendor->vendor_name) ? $batch->vendor->vendor_name : '-')) }} </td>
                    </tr>
                    <tr>
                        <td>Trainer Name</td>
                        <td> {{ ((isset($batch->trainer->trainer_name) ? $batch->trainer->trainer_name : '-')) }} </td>
                    </tr>
                    <tr>
                        <td>Total Employee Count</td>
                        <td> {{ count($batch->nominee) }} </td>
                    </tr>
                    <tr>
                        <td>Accepted Employee Count</td>
                        <td> {{ count($batch->nominee_accepted) }} </td>
                    </tr>
                    <tr>
                        <td>Rejected Employee Count</td>
                        <td> {{ count($batch->nominee_rejected) }} </td>
                    </tr>
                    @if($batch->approval == 0)
                    <tr>
                        <td>Approval</td>
                        <td>
                            <a href="{{url('/admin/batch/approval/1/').'/'.$batch->id}}" class="btn btn-success"> Approval</a>
                            <a href="{{url('/admin/batch/approval/2/').'/'.$batch->id}}" class="btn btn-danger"> Cancel</a>
                        </td>
                    </tr>
                    @else
                    <tr>
                        <td>Email</td>
                        <td>
                            batch.{{$batch->id}}@gmail.com
                        </td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td>
                            {{$batch->batch_password}}
                        </td>
                    </tr>
                    @endif
                    </tbody>
                    </table>

        </div>
    </div>
</div>
@endsection