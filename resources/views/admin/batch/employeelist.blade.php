@if(isset($employee) && $employee != null)

<div class="card-body">
    <div class="card-block">
        <table class="table table-borderless idslist" style="width:100%;" id="employee-table">
            <thead>
                <tr>
                    <th>Code</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Department</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($employee as $key => $value)
                <tr>
                    <td>{{$value->emp_code}}</td>
                    <td>{{$value->emp_name}}</td>
                    <td>{{$value->email}}</td>
                    <td>{{ (isset($value->department->department_name) ? $value->department->department_name : '')}}</td>
                    <td><input type="checkbox" name="ids[]" class="idchecklist" value="{{$value->id}}"></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


{!! Form::hidden('idsArr',null, ['class' => 'form-control','id'=>'idsArr']) !!}
{!! Form::hidden('datepicker',null, ['class' => 'form-control','id'=>'fdatepicker']) !!}
{!! Form::hidden('program_id',null, ['class' => 'form-control','id'=>'fprogram_id']) !!}
{!! Form::hidden('level',null, ['class' => 'form-control','id'=>'flevel']) !!}
{!! Form::hidden('vendor_id',null, ['class' => 'form-control','id'=>'fvendor_id']) !!}
{!! Form::hidden('trainer_id',null, ['class' => 'form-control','id'=>'ftrainer_id']) !!}                   
<div class="form-group pull-right">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary createupdatebtn']) !!}
</div>
@else
@endif


@push('js')

    <script>  
    
    sessionStorage.removeItem("checkedIds");
    $(document).on('click', '.idchecklist', function (e) {

        idsArr = sessionStorage.getItem("checkedIds");
        count = $('#count').text();
        if ($(this).is(':checked')) {
            count++;
            idsArr = ((idsArr != null) ? idsArr+'|'+this.value : this.value);
            sessionStorage.setItem("checkedIds", idsArr);
            
        } else {
            count--;
            var itemtoRemove = this.value;
            idsArr = idsArr.replace(itemtoRemove, '');
            sessionStorage.setItem("checkedIds", idsArr);
        }
        $('#count').html(count);
    });
    $('#batch_create_form').validate({        
        onkeyup: false,
        onclick: false,
        onfocusout: false,
        rules: {
            program_id: { required: true},
            date: { required: true},
            vendor_id: { required: true},
            trainer_id: { required: true},
            'ids[]': {
                required: true,
                minlength: 1
            }
        },
        messages: {
            date: "Please select a date",
            program_id: "Please select a program",
            vendor_id: "Please select a vendor",
            'ids[]': "Please select atleast one employee", 
            trainer_id:"Please select trainner"    
        },
        submitHandler: function (form) {
            datepicker = $('#datepicker').val();
            program_id = $('#program_id').val();
            level = $('#level').val();
            vendor_id = $('#vendor_id').val();
            trainer_id = $('#trainer_id').val();

            
            $('#fdatepicker').val(datepicker);
            $('#fprogram_id').val(program_id);
            $('#flevel').val(level);
            $('#fvendor_id').val(vendor_id);
            $('#ftrainer_id').val(trainer_id);

            idsArr = sessionStorage.getItem("checkedIds").split('|');
            var newidsArr = idsArr.filter(function(v){return v!==''});
            $('#idsArr').val(newidsArr);
            form.submit();
        },
        errorPlacement: function(error, element) 
        { 
            if ( element.is(":checkbox") ) 
            {
                error.appendTo( element.parents('.idslist') );
            }
            else 
            {  
                error.insertAfter( element );
            }
        }
    }); 
   
    </Script>
    
@endpush