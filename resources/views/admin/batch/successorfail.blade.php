<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Arvind</title>
        <style>
            .content-center {
            text-align: center !important;
            margin-top: 360px !important; }
        </style>
		<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	
	</head>
	<body style="margin:30px auto; width:850px;font-family: 'Montserrat', sans-serif;">
        
    <div class="card">
        <div class="card-body">
            <div class="content-center">
                <h1>{{$msg}}</h1>
            </div>
        </div>
    </div>
	</body>
</html>
