<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-12">
                <div class="col-md-3 floatleft">
                    {!! Form::label('date', 'Program Date :', ['class' => 'control-label']) !!}
                    {!! Form::text('date',null, ['class' => 'form-control batchsrch  datepicker searchval','placeholder' => 'Select Date','id' => 'datepicker']) !!} 
                </div>
                <div class="col-md-2 floatleft">
                    {!! Form::label('program_id', 'Program :', ['class' => 'control-label']) !!}
                    {!! Form::select('program_id',$program ,(isset($batch->program_id) ? $batch->program_id : null), ['class' => 'form-control batchsrch searchval','id' => 'program_id']) !!} 
                </div>
                <div class="col-md-2 floatleft">
                    {!! Form::label('vendor_id', 'Vendor :', ['class' => 'control-label']) !!}
                    {!! Form::select('vendor_id',$vendor ,(isset($batch->vendor_id) ? $batch->vendor_id : null), ['class' => 'form-control batchsrch searchval','id' => 'vendor_id']) !!} 
                </div>
                <div class="col-md-2 floatleft">
                    {!! Form::label('trainer_id', 'Trainer :', ['class' => 'control-label']) !!}
                    {!! Form::select('trainer_id',(isset($trainer) ? $trainer : [''=>'Select Trainer']) ,(isset($batch->trainer_id) ? $batch->trainer_id : null), ['class' => 'form-control batchsrch searchval','id' => 'trainer_id']) !!} 
                </div>                            
            </div>
        </div>
        <div class="pull-right">
            <span id="count">0</span>
        </div>
        <div class="row emp_srch">
            <div class="card-header">
                <h4 class="card-title">Employees</h4>
            </div>
            <div class="col-12">
                <div class="col-md-2 floatleft">
                    {!! Form::label('level', 'Level :', ['class' => 'control-label']) !!}
                    {!! Form::select('level',['0'=>'Select Level','1'=>'L1','2'=>'L2','3'=>'L3'] ,1, ['class' => 'form-control  searchval','id' => 'level']) !!} 
                </div>
                <div class="col-md-2 floatleft">
                    {!! Form::label('region_id', 'Region :', ['class' => 'control-label']) !!}
                    {!! Form::select('region_id',$region ,null, ['class' => 'form-control searchval','id' => 'region_id']) !!} 
                </div>
                <div class="col-md-2 floatleft">
                    {!! Form::label('location_id', 'Location :', ['class' => 'control-label']) !!}
                    {!! Form::select('location_id',[''=>'Select Location'] ,null, ['class' => 'form-control  searchval','id' => 'location_id']) !!} 
                </div>
                <div class="col-md-2 floatleft">
                    {!! Form::label('department_id', 'Department :', ['class' => 'control-label']) !!}
                    {!! Form::select('department_id',$department ,null, ['class' => 'form-control  searchval','id' => 'department_id']) !!} 
                </div>
                <div class="col-md-2 floatleft">
                    {!! Form::label('designation_id', 'Designation :', ['class' => 'control-label']) !!}
                    {!! Form::select('designation_id',$designation ,null, ['class' => 'form-control  searchval','id' => 'designation_id']) !!} 
                </div>
                <div class="col-md-2 floatleft">
                    {!! Form::label('division_id', 'Division :', ['class' => 'control-label']) !!}
                    {!! Form::select('division_id',$division ,null, ['class' => 'form-control  searchval','id' => 'division_id']) !!} 
                </div>
                
            </div>
        </div>
        <div id="emp_list">
            @include('admin.batch.employeelist')
        </div>
        
    </div>
</div>

@push('js')

<script>
    //$('.emp_srch').hide();
    $(".ui-datepicker-today span").addClass("ui-state-hover")
    var today = new Date();
    var tomorrow = new Date();
    tomorrow.setDate(today.getDate() + 1);
        $('.datepicker').pickadate({
            min: tomorrow,
            format: 'yyyy-mm-dd', 
            formatSubmit: 'yyyy-mm-dd'});

    $(function() {
    $(document).on('change', '#vendor_id', function (e) {
        var id = $('#vendor_id').val();
        if(id != '')
        {
            data = new Array();
            program_id = $('#program_id').val();   
            data.push({ name: "program_id", value: program_id });

            var url ="{{ url('admin/getTrainers/') }}";
            url = url + "/" + id;
            $.ajax({
                type: "get",
                url: url ,
                data : data,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                        data = JSON.parse( data );
                        var select = $('#trainer_id');
                        select.empty();
                        select.append('<option value="">Select Trainer</option>');
                        $.each(data,function(key, value)
                        {
                            select.append('<option value=' + key + '>' + value + '</option>');
                        });
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
        else 
        {
            var select = $('#trainer_id');
            select.empty();
            select.append('<option value="">Select Trainer</option>');
        }
        
    });
    $(document).on('change', '.searchval', function (e) {
        data = new Array();
        datepicker = $('#datepicker').val();
        program_id = $('#program_id').val();
        level = $('#level').val();
        vendor_id = $('#vendor_id').val();
        trainer_id = $('#trainer_id').val();

           
        data.push({ name: "date", value: datepicker });
        data.push({ name: "program_id", value: program_id });
        data.push({ name: "level", value: level });
        data.push({ name: "vendor_id", value: vendor_id });
        data.push({ name: "trainer_id", value: trainer_id });
        $('#count').html(0);
        $('.errBlog').html('');
        

        region_id = $('#region_id').val();
        location_id = $('#location_id').val();
        department_id = $('#department_id').val();
        designation_id = $('#designation_id').val();
        division_id = $('#division_id').val();

            if(region_id != '')
            {
                data.push({ name: "region_id", value: region_id });
            }
            if(location_id != '')
            {
                data.push({ name: "location_id", value: location_id });
            }
            if(department_id != '')
            {
                data.push({ name: "department_id", value: department_id });
            }
            if(designation_id != '')
            {
                data.push({ name: "designation_id", value: designation_id });
            }
            if(division_id != '')
            {
                data.push({ name: "division_id", value: division_id }); 
            }
        
        //console.log(data);
        var url ="{{ url('admin/batch/employeeTable/') }}";
        $.ajax({
            type: "get",
            url: url ,
            data : data,
            headers: {
                "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
            },
            success: function (data) {
                if(data.success == false)
                {
                    $('.emp_srch').hide();
                    var html = '<ul class="alert alert-danger">\
                        <li>'+ data.message +' </li>\
                        </ul>';
                    $('.errBlog').html(html);
                    $('#emp_list').html('');
                }
                else {
                    $('.emp_srch').show();
                    $('#emp_list').html(data.data);
                    $('.errBlog').html('');
                    checkedIds = sessionStorage.getItem("checkedIds");
                    if(checkedIds != null)
                    {
                        idsArr = checkedIds.split('|');
                        var newidsArr = idsArr.filter(function(v){return v!==''});
                        $('#count').html(newidsArr.length);
                        $('.idchecklist').each(function () {
                            $(this).prop("checked", ($.inArray($(this).val(), newidsArr) !== -1) );
                        });
                    }
                    
                }
            },
            error: function (xhr, status, error) {
                $('.emp_srch').hide();
                var erro = ajaxError(xhr, status, error);
                toastr.error('Action Not Procede!',erro)
            }
        });
        
        
    });
    $(document).on('change', '.batchsrch', function (e) {
        sessionStorage.removeItem("checkedIds");
        $('#count').html(0);
    });
    $(document).on('change', '#region_id', function (e) {
        var id = $('#region_id').val();
        if(id != '')
        {
            var url ="{{ url('admin/getLocation/') }}";
            url = url + "/" + id;
            $.ajax({
                type: "get",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                        data = JSON.parse( data );
                        var select = $('#location_id');
                        select.empty();
                        select.append('<option value="">Select Location</option>');
                        $.each(data,function(key, value)
                        {
                            select.append('<option value=' + key + '>' + value + '</option>');
                        });
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
        else 
        {
            var select = $('#location_id');
            select.empty();
            select.append('<option value="">Select Location</option>');
        }
        
    });
    }); 
</Script>
    
@endpush