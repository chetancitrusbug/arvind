@extends('layouts.admin') 
@section('title',"Employee") 
@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Employee Batch List</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="{{ url('/admin/employee') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <table class="table table-borderless" style="width:100%;" id="batch-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Date</th>
                        <th>Vendor</th>
                        <th>Trainner</th>
                        <th>Program</th>
                        <th>Approval</th>
                        <th>Status</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@push('js')

    <script>
        var id = "{{$id}}";
        var url ="{{ url('/admin/employee/listBatchDatatable') }}"+'/'+id;
        var auth_check = "{{ Auth::check() }}";

    
        datatable = $('#batch-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": false, 
            ajax: {
                url:url,
                type:"get"
            },
                columns: [
                    { data: 'id',name:'id',"searchable" : true}, 
                    { data: 'date',name:'date',"searchable" : true}, 
                    { data: 'vendor_name',name:'vendor_name',"searchable" : true},
                    { data: 'trainer_name',name:'trainer_name',"searchable" : true},
                    { data: 'program_name',name:'program_name',"searchable" : true}, 
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var approval = '';
                            if(o.approval == 0)
                            approval = 'No';
                            else if(o.approval == 2)
                            approval = "Cancelled";
                            else
                            approval = "Yes";
                            return approval;
                        }
                    },
                    { 
                        "data": null,
                        "name" : 'status',
                        "searchable": true,
                        "orderable": true,
                        "render": function (o) {
                            if(o.status == 0)
                            status = 'Inactive';
                            else
                            status = "Active";
                            return status;
                        }

                    }
                ]
        });
    </Script>
    
@endpush