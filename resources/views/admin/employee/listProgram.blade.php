@extends('layouts.admin') 
@section('title','Employee') 
@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Employee Program List</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="{{ url('/admin/employee') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <table class="table table-borderless" style="width:100%;" id="program-table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Classification</th>
                        <th>Status</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@push('js')

    <script>
        var id = "{{$id}}";
        var url ="{{ url('/admin/employee/listProgramDatatable') }}"+'/'+id;
        var auth_check = "{{ Auth::check() }}";

    
        datatable = $('#program-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]], 
            ajax: {
                url:url,
                type:"get"
            },
                columns: [
                    { data: 'program_name',name:'program_name',"searchable" : true},
                    { data: 'cat_name',name:'cat_name',"searchable" : true}, 
                    { data: 'classification_name',name:'classification_name',"searchable" : true},
                    { 
                        "data": null,
                        "name" : 'status',
                        "searchable": true,
                        "orderable": true,
                        "render": function (o) {
                            if(o.status == 0)
                            status = 'Inactive';
                            else
                            status = "Active";
                            return status;
                        }

                    }
                ]
        });
    </Script>
    
@endpush