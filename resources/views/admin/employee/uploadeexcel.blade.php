@extends('layouts.admin') 
@section('title',"Add Employees") 
@section('content')


<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Add New Employees</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <!-- button & search bar -->
                <a href="{{ url('/admin/employee') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            {!! Form::open(['route' => 'employee.upload.submit', 'class' => 'form-horizontal','id' => 'module_form_uploade','files'=>true]) !!}

                <div class="row">
		           	<div class="col-xs-12 col-sm-12 col-md-12">
		                <div class="form-group">
		                    {!! Form::label('uploadExcel','Upload Excel File:',['class'=>'col-md-3']) !!}
		                    <div class="col-md-9">
		                    {!! Form::file('uploadExcel', array('class' => 'form-control')) !!}
		                    {!! $errors->first('uploadExcel', '<p class="alert alert-danger">:message</p>') !!}
		                    </div>
		                </div>
		            </div>
		            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
		            	{!! Form::submit('Upload',['class'=>'btn btn-primary']) !!}
		            </div>
		        </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection