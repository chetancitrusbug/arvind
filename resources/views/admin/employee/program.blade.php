@extends('layouts.admin') 
@section('title',"Programs of Employee") 
@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Programs of Employee  # {{  $employee->emp_name or $employee->emp_code }}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="{{ url('/admin/employee') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">     
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Employee Name/Code</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> {{$employee->emp_name or $employee->emp_code}} </td>
                                <td> {{$employee->email}}</td>                                
                            </tr>   
                        </tbody>
                    </table>
                </div>
            </div>
            @foreach($category as $cat)
            <div class="category-selected">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>{{ $cat->cat_name }}</h3>
                        @php $colorclass = ["bg-purple", "bg-red", "bg-green", "bg-skyblue",""];
                        $curr = 0; @endphp
                        @foreach($cat->program as $program)
                            <span class="tags {{ $colorclass[$curr] }} ">{{ $program->program_name }}</span>
                            @php
                                $curr++;
                                if ($curr == count($colorclass)) {
                                    $curr = 0;
                                }
                            @endphp
                        @endforeach
                    </div>
                </div>
            </div>
            @endforeach
            @if($select_status == 1)
            <div class="row">
                <div class="col-3">
                    <a href="{{ url('/admin/employee/'.$employee->id.'/programs') }}?status=approve" title="Approve"><button class="btn btn-success btn-sm"> Approve </button></a>
                </div>
                <div class="col-3">
                    <a href="{{ url('/admin/employee/'.$employee->id.'/programs') }}?status=denied" title="Cancel"><button class="btn btn-danger btn-sm">Cancel </button></a>
                </div>
            </div>
            @elseif($select_status == 2)
                <button class="btn btn-danger btn-sm">Cancelled</button>
            @else
                <button class="btn btn-success btn-sm"> Approved </button>
            @endif
        </div>
    </div>
</div>
@endsection