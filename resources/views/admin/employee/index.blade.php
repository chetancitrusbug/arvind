@extends('layouts.admin') 
@section('title',"Employee") 
@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Employee</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">

                    <a href="{{ url('/admin/employee/create') }}" class="btn btn-success btn-sm"
                       title="Add New Employee">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>
                    @if(Auth::user()->hasRole('SU'))
                    <a href="{{ route('employee.upload') }}" class="btn btn-success btn-sm"
                    title="@lang('forms.label.uploadexcel') @lang('forms.label.program')">
                     <i class="fa fa-plus" aria-hidden="true"></i> @lang('forms.label.uploadexcellabel')
                 </a>
                 @endif
                
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
                <table class="table table-borderless" style="width:100%;" id="employee-table">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Department</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
        </div>
    </div>
</div>
@endsection
@push('js')

    <script>
        var url ="{{ url('/admin/employee/datatable') }}";
        var edit_url = "{{ url('/admin/employee') }}";
        var auth_check = "{{ Auth::check() }}";
    
        datatable = $('#employee-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]], 
            ajax: {
                url:url,
                type:"get",
                data: function (d)  {
                    d.areas = $('#areas').val();
                }
            },
                columns: [
                    { data: 'emp_code',name:'emp_code',"searchable" : true}, 
                    { data: 'emp_name',name:'emp_name',"searchable" : true}, 
                    { data: 'email',name:'email',"searchable" : true}, 
                    { data: 'department_name',name:'department_name',"searchable" : true}, 
                    { 
                        "data": null,
                        "name" : 'status',
                        "searchable": true,
                        "orderable": true,
                        "render": function (o) {
                            var role = "{{ Auth::user()->roles[0]->name }}";
                            if(role == "manager"){
                                if(o.emp_programs.length > 0){
                                    if(o.emp_programs[0].status == 1){
                                        status = '<button class="btn btn-primary btn-xs">Programs Updated</button>';
                                    }else{
                                        if (o.emp_programs[0].status == 2){
                                            status = "<button class='btn btn-danger btn-xs'>Cancelled</button>";
                                        }else{
                                            status = "<button class='btn btn-success btn-xs'>Approved</button>";
                                        }
                                    }
                                }else{
                                    status = "<button class='btn btn-warning btn-xs'>Pending</button>";
                                }
                                return status;
                            }else{
                                if(o.status == 0)
                                    status = '<a href="'+edit_url+'/'+o.id+'?status=1" title="Active"><button class="btn btn-danger btn-xs"> @lang("forms.label.inactive")</button></a>';
                                else
                                    status = "<a href='"+edit_url+"/"+o.id+"?status=0' data-id="+o.id+" title='Inactive'><button class='btn btn-success btn-xs'> @lang('forms.label.active')</button></a>";
                                return status;
                            }
                        }
                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var role = "{{ Auth::user()->roles[0]->name }}";
                            if(role == "manager"){
                                var v = "";
                                if(o.emp_programs.length > 0){
                                    v = "<a href='"+edit_url+"/"+o.id+"/programs' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info btn-sm' title='Programs' ><i class='fa fa-eye' ></i></button></a>&nbsp;";
                                }else{
                                    v = "";
                                }
                                return v ;
                            }else{
                                var e=""; var d= "";
                                e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info btn-sm' title='Edit' ><i class='fa fa-edit' ></i></button></a>&nbsp;";  
                                d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" ><button class='btn btn-danger btn-sm' title='Delete' ><i class='fa fa-trash-o' aria-hidden='true'></i></button></a>&nbsp;"; 
                                p = "<a href='"+edit_url+"/programs/"+o.id+"' title='List'><button class='btn btn-info btn-xs'> Programs </button></a>&nbsp;";
                                b = "<a href='"+edit_url+"/batch/"+o.id+"'  title='List'><button class='btn btn-info btn-xs'> Batch </button></a>&nbsp;";
                                return e+d+p+b;
                            }
                        }
                    }
                ]
        });

        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            url = "{{url('admin/employee')}}/" + id;
            var r = confirm("Are you sure you want to Delete Employee?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success(data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            }
        });


    </Script>
    
@endpush