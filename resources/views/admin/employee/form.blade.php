
<div class="form-group{{ $errors->has('emp_name') ? ' has-error' : ''}}">
    {!! Form::label('emp_name', '* Name: ', ['class' => 'control-label']) !!} 
    {!! Form::text('emp_name', null, ['class' => 'form-control col-md-4']) !!} {!! $errors->first('emp_name', '
    <p class="help-block">:message</p>') !!}
</div>
@if(isset($employee->email))
<div class="form-group{{ $errors->has('emp_code') ? ' has-error' : ''}}">
    {!! Form::label('emp_code', '* Employee Code: ', ['class' => 'control-label']) !!} 
    {!! Form::text('emp_code', null, ['class' => 'form-control col-md-4','disabled' => 'disabled']) !!} {!! $errors->first('emp_code', '
    <p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', '* Email: ', ['class' => 'control-label']) !!} 
    {!! Form::text('email', null, ['class' => 'form-control col-md-4','disabled' => 'disabled']) !!} 
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
@else
<div class="form-group{{ $errors->has('emp_code') ? ' has-error' : ''}}">
    {!! Form::label('emp_code', '* Employee Code: ', ['class' => 'control-label']) !!} 
    {!! Form::text('emp_code', null, ['class' => 'form-control col-md-4']) !!} {!! $errors->first('emp_code', '
    <p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', '* Email: ', ['class' => 'control-label']) !!} 
    {!! Form::text('email', null, ['class' => 'form-control col-md-4']) !!} 
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
@endif   

<div class="form-group{{ $errors->has('program') ? ' has-error' : ''}}">
    {!! Form::label('program', '* Program:', ['class' => 'control-label']) !!}
    </br>
    {!! Form::select('program[]', $program, null, ['id' => 'program','class' => 'form-control col-md-4' ,'multiple' => 'multiple']) !!}
    {!! $errors->first('program', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('mobile_no') ? ' has-error' : ''}}">
    {!! Form::label('mobile_no', '* Mobile No: ', ['class' => 'control-label']) !!} 
    {!! Form::text('mobile_no', null, ['class' => 'form-control col-md-4']) !!} {!! $errors->first('mobile_no', '
    <p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('reporting_manager') ? ' has-error' : ''}}">
    {!! Form::label('reporting_manager', '* Reporting Manager Code: ', ['class' => 'control-label']) !!} 
    {!! Form::text('reporting_manager', null, ['class' => 'form-control col-md-4']) !!} {!! $errors->first('reporting_manager', '
    <p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('region_id') ? ' has-error' : ''}}">
    {!! Form::label('region_id', 'Region:', ['class' => 'control-label']) !!}
    {!! Form::select('region_id', $region, isset($employee->region_id) ? $employee->region_id : [], ['id' => 'region_id','class' => 'form-control col-md-4']) !!}
    {!! $errors->first('region_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('location_id') ? ' has-error' : ''}}">
    {!! Form::label('location_id', 'Location:', ['class' => 'control-label']) !!}
    {!! Form::select('location_id', $location, isset($employee->location_id) ? $employee->location_id : [], ['id' => 'location_id','class' => 'form-control col-md-4']) !!}
    {!! $errors->first('location_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('department_id') ? ' has-error' : ''}}">
    {!! Form::label('department_id', 'Department:', ['class' => 'control-label']) !!}
    {!! Form::select('department_id', $department, isset($employee->department_id) ? $employee->department_id : [], ['id' => 'department_id','class' => 'form-control col-md-4']) !!}
    {!! $errors->first('department_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('designation_id') ? ' has-error' : ''}}">
    {!! Form::label('designation_id', 'Designation:', ['class' => 'control-label']) !!}
    {!! Form::select('designation_id', $designation, isset($employee->designation_id) ? $employee->designation_id : [], ['id' => 'designation_id','class' => 'form-control col-md-4']) !!}
    {!! $errors->first('designation_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('division_id') ? ' has-error' : ''}}">
    {!! Form::label('division_id', 'Division:', ['class' => 'control-label']) !!}
    {!! Form::select('division_id', $division, isset($employee->division_id) ? $employee->division_id : [], ['id' => 'division_id','class' => 'form-control col-md-4']) !!}
    {!! $errors->first('division_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('brand_id') ? ' has-error' : ''}}">
    {!! Form::label('brand_id', 'Brand:', ['class' => 'control-label']) !!}
    {!! Form::select('brand_id', $brand, isset($employee->brand_id) ? $employee->brand_id : [], ['id' => 'brand_id','class' => 'form-control col-md-4']) !!}
    {!! $errors->first('brand_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('level') ? 'has-error' : ''}}">
    {!! Form::label('level', '* Level', ['class' => 'control-label']) !!}
    {!! Form::select('level',[''=>'Select Level','1'=>'L1','2'=>'L2','3'=>'L3'] ,null, ['class' => 'form-control col-md-4']) !!} 
    {!! $errors->first('level','<p class="help-block with-errors">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', '* Status', ['class' => 'control-label']) !!}
    {!! Form::select('status',['1'=>'Active','0'=>'Inactive'] ,@$employee->status, ['class' => 'form-control col-md-4']) !!} 
    {!! $errors->first('status','<p class="help-block with-errors">:message</p>') !!}
</div>
<div class="form-group">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
@push('js')
<script>
$(function() {
    $('#program').select2();
    $(document).on('change', '#region_id', function (e) {
        var id = $('#region_id').val();
        if(id != '')
        {
            var url ="{{ url('admin/getLocation/') }}";
            url = url + "/" + id;
            $.ajax({
                type: "get",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                        data = JSON.parse( data );
                        var select = $('#location_id');
                        select.empty();
                        select.append('<option value="">Select Location</option>');
                        $.each(data,function(key, value)
                        {
                            select.append('<option value=' + key + '>' + value + '</option>');
                        });
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
        else 
        {
            var select = $('#location_id');
            select.empty();
            select.append('<option value="">Select Location</option>');
        }
        
    });
}); 
</script>
@endpush