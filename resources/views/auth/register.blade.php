{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}

@extends('layouts.frontend')    

@section('page-title','REGISTER')

@section('content')

    <div class="choose-div-box m0 clearfix">
        <h1>Register</h1>
        <p class="p-heading">Registering is quick and allows you to begin submitting data.  Arvind University will keep
        track of your entries and will award you membership points to be used for rewards.  Arvind University will not provide your email address to any other party and will only notify you of account related activity.</p>
    </div>
    
    <!-- Main content -->
    <section class="content">
        <div class="form-div clearfix">
            <div class="container">
                <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                            @csrf
                <div class="form-register">
                    <div class="form-box clearfix">
                        
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group clearfix">

                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-5"><label for="username">Desired username: </label></div>
                                        <div class="col-md-8 col-sm-7">

                                                <input id="name" type="text" class="form-control input-95 mb15 {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="username" />

                                               
{{-- 
                                            <input type="text" class="form-control input-95 mb15" id="username" placeholder="username"> --}}
                                            <span class="req">*</span>
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                        </div>
                                    </div>
                                </div><!-- end of col -->
                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-5"><label for="password">Password:</label></div>
                                        <div class="col-md-8 col-sm-7">

                                                <input id="password" type="password" class="form-control input-95  mb15 {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="password" />

                                               

                                            {{-- <input type="password" class="form-control input-95  mb15" id="password" placeholder="Password"> --}}
                                            
                                            <span class="req">*</span>

                                            @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                        </div>
                                    </div>
                                </div><!-- end of col -->
                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-5"><label for="password">Confirm password:</label></div>
                                        <div class="col-md-8 col-sm-7">

                                            <input id="password-confirm" type="password" class="form-control input-95  mb15" name="password_confirmation" required placeholder="Confirm Password" />

                                            {{-- <input type="password" class="form-control input-95  mb15" id="password" placeholder="Confirm Password"> --}}
                                            
                                            <span class="req">*</span>
                                        </div>
                                    </div>
                                </div><!-- end of col -->
                            </div><!-- end of form-group -->
                        </div>	
                        
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group clearfix">
                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-5"><label for="email">Email address:</label></div>
                                        <div class="col-md-8 col-sm-7">

                                                <input id="email" type="email" class="form-control input-95 mb15 {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email address" />

                                            {{-- <input type="email" class="form-control input-95 mb15" id="email" placeholder="Email address"> --}}
                                            
                                            <span class="req">*</span>
                                            @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                        </div>
                                    </div>
                                </div><!-- end of col -->
                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-5"><label for="name">Favorite location: </label></div>
                                        <div class="col-md-8 col-sm-7">
                                            @php $location = App\Location::where('status',1)->pluck('name', 'id')->Prepend('Select Location',''); 
                                            @endphp
                                            {!! Form::select('location_id', $location, null, ['class' => 'selectpicker input-95 mb15','data-live-search' => 'true']) !!} 
                                            {{-- <select class="selectpicker input-95 mb15">
                                            <option>Atlantic City</option>
                                            <option>Offered</option>											
                                            </select>
                    
                                        {{-- <span class="req">*</span> --}}
                                        </div>
                                    </div>
                                    {{-- <div class="row">  
                                        <div class="g-recaptcha" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}"></div>
                                    </div> --}}
                                </div><!-- end of col -->
                                
                            </div>
                        </div>	
                        
                    </div>
                </div><!-- End of form-register -->
               
                
            </div>	
            
            <div class="form-group text-center mt30 clearfix">
                <div class="col-md-12 col-sm-12">
                     <input name="" type="submit" class="btn btn-primary text-center" value="Register">
                </div><!-- end of col -->
            </div><!-- end of form-group -->
        </form>
            <div class="clearfix text-center"><a href="{{ url('/') }}" class=""><i class="fa fa-angle-left "></i> Back to Home page</a></div>
        </div>	
    </section><!-- end of content section -->


@endsection

@push('js')
{{-- <script>
    $('#submit').click(function () {
        var captcha =  grecaptcha.getResponse();
  
        if(captcha == ''){
            $(".g-recaptcha").addClass("error_message");
            return false;
        }else{
            $(".g-recaptcha").removeClass("error_message");
            return true;
        }
  });
</script> --}}
@endpush

