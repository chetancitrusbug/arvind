@extends('layouts.admin')


@section('title','Access Denied')


@section('content')
<div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">
                    <h4 class="card-title">Permission Denied</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <!-- button & search bar -->
                   <button class="btn btn-warning btn-xs">Oops!! You don't have permission for access this page.</button>
                            
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card-block">
            
                 <!-- Put Table structure & Forms here -->
                
            </div>
        </div>
    </div>
@endsection

