{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}

@extends('layouts.frontend')

@section('page-title','AREA')

@section('content')

<div class="choose-div-box clearfix">
    
    {!! Form::open(['method' => 'GET', 'url' => '/admin/products-data','class' =>'col-md-6']) !!} 
    <label class="inline">Choose Area:</label>
     {!! Form::select('areas',$areas,null, ['class' => 'selectpicker inline','data-live-search' => 'true' , 'id' =>'areas' ]) !!}
    {!! Form::close() !!}
    
    @auth
    <span class="inline add-link"><a href="{{ url('/area') }}" class="inline-link"><i class="fa fa-plus"></i> Add New Area</a></span>
    @endauth
</div>

 <!-- Main content -->
<section class="content">

    <div class="table-responsive clearfix">
    <table class="table table-hover cust-tbl" style="width:100%" id="casino-table">
    <thead>
        <tr>
            <th></th>
            <th width="20%">Casino Name</th>
            <th width="10%">Min. Bet</th>
            <th width="20%">Max Bet</th>
            <th width="10%">Odds</th>
            <th width="10%">Machines</th>
            {{-- <th width="10%">Machine Data</th> --}}
            <th width="10%">Last Update</th>
            <th width="10%">Status</th>
        </tr>
    </thead> 
    </table>
    </div>

</section><!-- end of content section -->

@endsection

@push('js')

    <script>
        var login_url ="{{ route('login') }}";
        var url ="{{ url('/home-data') }}";
        var edit_url = "{{ url('/casino') }}";
        var auth_check = "{{ Auth::check() }}";

    
        datatable = $('#casino-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]], 
            ajax: {
                url:url,
                type:"get",
                data: function (d)  {
                    d.areas = $('#areas').val();
                }
            },
                columns: [
                    { data: 'created_at',name : 'created_at',"visible":false},
                    { 
                        "data":null,
                        "name":'casino.name',
                        "searchable": true,
                        "orderable": true,
                        "render": function (o) {                    
                            return o.casino_name;
                        }

                    },
                    { 
                        "data": null,
                        "name" : 'min_bet',
                        "searchable": true,
                        "orderable": true,
                        "render": function (o) {
                            var bet="";
                            if(auth_check){
                                bet = o.min_bet ;
                            }else{
                                bet= "<a href='"+login_url+"'> Login </a>" ; 
                            }                            
                            return bet;
                        }

                    },
                    { 
                        "data": 'max_bet',
                        "name" : 'max_bet',
                        "searchable": true,
                        "orderable": true,
                    },
                    { 
                        "data": null,
                        "name" : 'odds',
                        "searchable": true,
                        "orderable": true,
                        "render": function (o) {                            
                            var odds="";
                            if(auth_check){
                                odds = o.odds ;
                            }else{
                                odds = "<a href='"+login_url+"'> Login </a>" ; 
                            }                            
                            return odds;
                        }
                    },
                    { 
                        "data": null,
                        "name" : 'machine_offered',
                        "searchable": true,
                        "orderable": true,
                        "render": function (o) {                            
                            return o.machine_offered;
                        }
                    },
                 
                    { 
                        "data": null,
                        "name" : 'date_data_observed',
                        "searchable": true,
                        "orderable": true,
                        "render": function (o) {                            
                            return o.date_data_observed;
                        }
                    },
                    @guest
                        { data: null,name:null,visible:false},
                    @endguest
                    @auth
                        { 
                            "data": null,
                            "searchable": false,
                            "orderable": false,
                            "render": function (o) {
                                var e=""; 
                                    e= "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" class='edit-item' ><button class='button-link'>Update</button></a>&nbsp;";   
                                return e;
                            }

                        }
                    @endauth
                ]
        });


        $('#areas').change(function() {
            datatable.draw();
        });

    </Script>
    
@endpush




