<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Arvind University</title>
		<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	
	</head>
	<body style="margin:30px auto; width:850px;font-family: 'Montserrat', sans-serif;">
		<div style="background:#000; padding:20px; text-align:center;">
			<img class="brand_icon" src="{{asset('assets/images/logo_lg.png')}}" alt="logo" style="width: 75px;" />
		</div>
		<div style="align:center; border:1px solid #ccc; padding-top:30px; padding-bottom:40px">
			<h2 style="margin-bottom: 35px;font-size: 25px;color:#000; font-size: 20px; text-align: center;">Batch Created!</h2>
			<table class="item_detail" style="border-collapse: collapse; width: 100%; text-align: center; margin:0px auto 20px auto;display: table;">
				<tr class="image-line" style="background: transparent;border: none !important;">
					<td colspan="3"  class="bottom-4" style="text-align: center;padding: 10px 20px;font-size: 15px;padding: 0;">
						<img src="{{asset('assets/images/top.jpg')}}" style="width: 100%;margin-bottom:-4px;">
					</td>
				</tr>
				<tr class="image-line" style="background: transparent;border: none !important;">
					<td colspan="6" class="item_tbl" style="text-align: center;padding: 25px 0px !important;background: #ededed;padding: 10px 20px;font-size: 15px;padding: 0;">
						<table class="tbl_order" style="background-color: #EDEDED;color: #000;border-bottom: 1px dashed #d3d3d3;width:100%;">
								<tbody>
									<tr>
										<td colspan="2" align="top" valign="top" style="background: #eeeeee; padding:0px 25px 10px; font-size: 15px;font-family: 'Lato', 'Arial';color:#9a9a9a;line-height: 1.4;">
										<p style="margin-top: 0;">Hello {{$trainnerName}},
										</p>
										<p style="margin-top: 0;">You are removed from batch.Check below all information about batch.
											</p>
										</td>                      
									</tr>
									<tr>
										<td align="top" valign="top" style="background: #eeeeee; padding:0px 25px 10px; font-size: 15px;font-family: 'Lato', 'Arial';color:#9a9a9a;line-height: 1.4;">
											<p style="margin-top: 0;"><a href="#" style="font-weight: 700;color: #00a3ac;margin: 20px 0;text-decoration: none;">Batch Date </a></p>
										</td>
										<td align="top" valign="top" style="background: #eeeeee; padding:0px 25px 10px; font-size: 15px;font-family: 'Lato', 'Arial';color:#9a9a9a;line-height: 1.4;">
											<p style="margin-top: 0;text-align: right;"><a href="#" style="font-weight: 700;color: #00a3ac;margin: 20px 0;text-decoration: none;">{{$date}}</a></p>
										</td>                            
									</tr>
									<tr>
										<td align="top" valign="top" style="background: #eeeeee; padding:0px 25px 10px; font-size: 15px;font-family: 'Lato', 'Arial';color:#9a9a9a;line-height: 1.4;">
											<p style="margin-top: 0;"><a href="#" style="font-weight: 700;color: #00a3ac;margin: 20px 0;text-decoration: none;">Program Name </a></p>
										</td>
										<td align="top" valign="top" style="background: #eeeeee; padding:0px 25px 10px; font-size: 15px;font-family: 'Lato', 'Arial';color:#9a9a9a;line-height: 1.4;">
											<p style="margin-top: 0;text-align: right;"><a href="#" style="font-weight: 700;color: #00a3ac;margin: 20px 0;text-decoration: none;">{{$programName}}</a></p>
										</td>                            
									</tr>
									
									<tr>
										<td align="top" valign="top" colspan="2" style="background: #eeeeee; padding:0px 25px 10px; font-size: 15px;font-family: 'Lato', 'Arial';color:#9a9a9a;line-height: 1.4;">
											<p style="margin-bottom: 0;">Thank you,</p>
											<p style="margin-top: 0;">Arvind University</p>
										</td>
									</tr>
									
								</tbody>
						</table>
					</td>
				</tr>
				<tr class="image-line" style="background: transparent;border: none !important;">
					<td colspan="6"  class="top-4" style="text-align: center;top: -5px; position: relative;padding: 10px 20px;font-size: 15px;padding: 0;">
						<img src="{{asset('assets/images/bottom.jpg')}}" style="width: 100%;margin-top:-4px;">
					</td>
				</tr>
			</table>
			
			<h4 style="margin-top: 35px;color: #333333; margin-left: 23px;"></h4>
			
		</div>
		<div class="footer" style="background:#333333; color:#ffffff; padding:10px;">
			<!-- <p>Copyright © 2017 All rights reserved</p> 
			<a href="https://www.be-help.com">www.be-help.com</a>-->
		</div>
	</body>
</html>
