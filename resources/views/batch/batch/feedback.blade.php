@extends('layouts.employeeLayout') 
@section('title',"Batch Feedback") 
@section('content')
<center><h2 class="card-title">Batch #{{$batch->id}} Feedback</h2></center>
<div class="card feedbackform">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Batch #{{$batch->id}}</h4>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>ID</th>
                            <td>#{{$batch->id}}</td>
                        </tr>
                        <tr>
                            <th>Date</th>
                            <td>{{$batch->date}}</td>
                         </tr>
                        <tr>
                            <th>Vendor</th>
                            <td>{{$batch->vendorName}}</td>
                        </tr>
                        <tr>
                            <th>Trainner</th>
                            <td>{{$batch->trainnerName}}</td>
                        </tr>
                        <tr>
                            <th>Program</th>
                            <td>{{$batch->programName}}</td>
                        </tr>
                        <tr>
                            <th>Employee</th>
                            <td>
                            <div class="form-group">
                                <input type="text" class="form-control col-sm-3" name="emp_code" id="emp_code" placeholder="Enter employee code">
                            </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="employeeDetails">
        
    </div>
</div>
@endsection
@push('js')

    <script>
        $('.employeeDetails').hide();
        var url ="{{ url('/batchfeedback/getEmployee') }}";    
        $(document).on('focusout', '#emp_code', function (e) {
            var emp_code = $('#emp_code').val();
            var batchId = "{{$batch->id}}";
                $.ajax({
                    type: "get",
                    url: url ,
                    data:{emp_code:emp_code,batchId:batchId},
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        $('.employeeDetails').show();
                        $('.employeeDetails').html(data.data);
                        toastr.success(data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            
        });

    </Script>
    
@endpush