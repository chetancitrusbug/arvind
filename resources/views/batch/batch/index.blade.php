@extends('layouts.admin') 
@section('title',"batch") 
@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Batch #{{$batch->id}}</h4>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>ID</th>
                            <td>#{{$batch->id}}</td>
                        </tr>
                        <tr>
                            <th>Date</th>
                            <td>{{$batch->date}}</td>
                         </tr>
                        <tr>
                            <th>Vendor</th>
                            <td>{{$batch->vendorName}}</td>
                        </tr>
                        <tr>
                            <th>Trainner</th>
                            <td>{{$batch->trainnerName}}</td>
                        </tr>
                        <tr>
                            <th>Program</th>
                            <td>{{$batch->programName}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">   
            <div class="form-group pull-right">
                <input type="text" class="form-control" name="emp_code" id="emp_code" placeholder="Add new Employee">
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <table class="table table-borderless" style="width:100%;" id="batch-table">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group pull-right">
            <input type="button" class="btn btn-success" name="feedbackbtn" id="feedbackbtn" value="Send Feedback Form ">
        </div>
    </div>
</div>
@endsection
@push('js')

    <script>
        var url ="{{ url('/batchuser/batch/datatable') }}";    
        datatable = $('#batch-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": false, 
            ajax: {
                url:url,
                type:"get"
            },
                columns: [
                    { data: 'emp_code',name:'emp_code',"searchable" : true}, 
                    { data: 'emp_name',name:'emp_name',"searchable" : true}, 
                    { data: 'email',name:'email',"searchable" : true}, 
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var c=""; 
                            if(o.attendence_status == 0)
                            c = "<input type='checkbox' class='attendence' value='"+o.id+"' >";  
                            else
                            c = "<input type='checkbox' class='attendence' value='"+o.id+"' checked>";
                            return c;
                        }

                    }
                ]
        });
        /*$(document).ready( function(){
            $('.attendence').checkboxpicker();
        });*/
        $(document).on('click', '.attendence', function (e) {
            var id = $(this).attr('value');
            var batchId = "{{$batch->id}}";
            url = "{{url('batchuser/batch/attendence')}}";
            if (this.checked) {
                action = 1;
            }
            else{
                action = 0;
            }
                $.ajax({
                    type: "post",
                    url: url ,
                    data:{action:action,empid:id,batchId:batchId},
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success(data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            
        });

        $(document).on('focusout', '#emp_code', function (e) {
            var emp_code = $('#emp_code').val();
            console.log(emp_code);
            if(emp_code != '')
            {
                var batchId = "{{$batch->id}}";
                url = "{{url('batchuser/batch/addEmployee')}}";
                $.ajax({
                    type: "post",
                    url: url ,
                    data:{emp_code:emp_code,batchId:batchId},
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        $('#emp_id').val('');
                        datatable.draw();
                        toastr.success(data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            }
            
            
        });
        
        $(document).on('click', '#feedbackbtn', function (e) {
            var batchId = "{{$batch->id}}";
            url = "{{url('batchuser/batch/sendFeedbackForm')}}";
                $.ajax({
                    type: "post",
                    url: url ,
                    data:{batchId:batchId},
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        toastr.success(data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            
        });

    </Script>
    
@endpush