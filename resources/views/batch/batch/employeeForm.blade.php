<h2>Employee Details</h2>
{!! Form::open(['url' => '/batchfeedback/feedbackForm', 'class' => 'form-horizontal']) !!}
<input type="hidden" name="employee_id" value="{{$employee->id}}">
<input type="hidden" name="batch_id" value="{{$batch_id}}">
<table class="table table-borderless">
    <tbody>
        <tr>
            <td>Id</td>
            <td>#{{ $employee->id }}</td>
        </tr>
        <tr>
            <td>Code</td>
            <td>{{ $employee->emp_code }}</td>
        </tr>
        <tr>
            <td>Name</td>
            <td> {{ $employee->emp_name }} </td>
        </tr>
        <tr>
            <td>Email</td>
            <td> {{ $employee->email }} </td>
        </tr>
        <tr>
            <td>Mobile No</td>
            <td> {{ $employee->mobile_no }} </td>
        </tr>
        <tr>
            <td>Reporting Manager</td>
            <td> {{ $employee->reporting_manager }} </td>
        </tr>
        <tr>
            <td>Department Name</td>
            <td> {{ ((isset($employee->department->department_name) ? $employee->department->department_name : '-')) }} </td>
        </tr>
        <tr>
            <td></td>
            <td> <input type="submit" class="btn btn-success" value="Submit"> </td>
        </tr>
    </tbody>
</table>


{!! Form::close() !!}