@extends('layouts.employeeLayout') 
@section('title',"Batch Feedback") 
@section('content')
<center><h2 class="card-title">Batch #{{$data['batch_id']}} Feedback</h2></center>
<div class="card feedbackform">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Batch #{{$data['batch_id']}}</h4>
                <br>
            </div>
        </div>
        <div class="card-body">
            <div class="card-block">
                <!-- Put Table structure & Forms here -->
                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif 
                {!! Form::open(['url' => '/batchfeedback/feedbackStore']) !!}
                <input type="hidden" name="batch_id" value={{$data['batch_id']}}>
                <input type="hidden" name="employee_id" value={{$data['employee_id']}}>
                <table class="table table-borderless" style="width:100%;" id="users-table">
                    <tbody>
                        @foreach($feedbackCategory as $catkey => $catvalue)
                            <tr>  
                                <th colspan="2"><b>Category#{{$catvalue->feedback_category_name}}</b></th>
                            </tr>
                            @foreach($feedbackQuestion as $quekey => $quevalue)
                            @if($catvalue->id == $quevalue->feedback_category_id)
                            <tr>  
                                <td>{{$quevalue->feedback_question}}</td>
                                <td>
                                    <input name="question{{$quevalue->id}}" type="radio" value="1" checked>1
                                    <input name="question{{$quevalue->id}}" type="radio" value="2">2
                                    <input name="question{{$quevalue->id}}" type="radio" value="3">3
                                    <input name="question{{$quevalue->id}}" type="radio" value="4">4
                                    <input name="question{{$quevalue->id}}" type="radio" value="5">5
                                </td>
                                {!! $errors->first('question{{$quevalue->id}}','<p class="help-block with-errors">:message</p>') !!}
                            </tr
                            @endif
                            @endforeach
                            @endforeach
                            <tr>
                                <td></td>
                                <td> <input type="submit" class="btn btn-success" value="Submit"> </td>
                            </tr>
                        </tbody>
                    </table>
                    
                
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')

    <script>
    </Script>
    
@endpush