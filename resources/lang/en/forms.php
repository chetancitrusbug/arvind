<?php

return [
    'label'  => [
        'inactive' => 'Inactive',
        'active'    => 'Active',
        'create' => 'Create',
        'edit' => 'Edit',
        'update' => 'Update',
        'add_new' => 'Add New',
        'create_new' => 'Create New',
        'back' => 'Back',
        'employee' => 'Employee',
        'trainer' => 'Trainner',
        'vendor' => 'Vendor',
        'program' => 'Program',
        'uploadexcel' => 'Upload Excel For Program and Employee',
        'uploadexcellabel' => 'Upload Excel'
    ],
];
